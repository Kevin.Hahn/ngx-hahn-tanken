import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { Angulartics2Module } from 'angulartics2';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';

const modules = [CommonModule, FormsModule, ReactiveFormsModule, NoopAnimationsModule, RouterTestingModule, HttpClientModule, MaterialModule];

@NgModule({
  imports: [
    ...modules,
    Angulartics2Module.forRoot({
      pageTracking: {
        clearQueryParams: true,
      },
      ga: {
        anonymizeIp: true,
      },
    }),
  ],
  exports: [...modules],
})
export class TestDependenciesModule {}
