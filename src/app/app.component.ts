import { CdkScrollable, ScrollDispatcher } from '@angular/cdk/overlay';
import { Component, NgZone, OnInit } from '@angular/core';
import { FeatureflagService } from '@core/services/featureflag.service';
import { TranslateService } from '@ngx-translate/core';
import { Angulartics2GoogleAnalytics } from 'angulartics2';
import { delay, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { AuthService } from './modules/auth/services/auth.service';
import { DialogService } from './modules/core/services/dialog.service';
import { StationHistoryService } from './modules/core/services/station-history.service';
import { StationService } from './modules/core/services/station.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  private readonly SHRINK_TOP_SCROLL_POSITION = 30;
  shrinkToolbar = false;
  currentLanguage = 'Deutsch';
  isLoginEnabled = false;
  version: string = environment.version;

  constructor(
    public stationService: StationService,
    private scrollDispatcher: ScrollDispatcher,
    private ngZone: NgZone,

    angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
    public historyService: StationHistoryService,
    private translate: TranslateService,
    public authService: AuthService,
    private dialogService: DialogService,
    public featureflags: FeatureflagService
  ) {
    angulartics2GoogleAnalytics.startTracking();
    translate.setDefaultLang('de');
    translate.use('de');
  }

  ngOnInit() {
    this.featureflags.isFeatureEnabled('no_login_functionality').subscribe((r) => (this.isLoginEnabled = r));

    this.scrollDispatcher
      .scrolled()
      .pipe(
        delay(1000),
        map((event: CdkScrollable) => event.getElementRef().nativeElement.scrollTop)
      )
      .subscribe((scrollTop) => this.ngZone.run(() => (this.shrinkToolbar = scrollTop > this.SHRINK_TOP_SCROLL_POSITION)));

    this.historyService.isOnline().subscribe({
      next: () => (this.historyService.apiOnline = true),
      error: () => (this.historyService.apiOnline = false),
    });

    this.historyService.getLastUpdated().subscribe({
      next: (status) => {
        this.historyService.lastStatusDate = status.date;
        this.historyService.lastWeekDate = new Date(new Date(status.date).setDate(new Date(status.date).getDate() - 7));
        this.historyService.lastTwoWeekDate = new Date(new Date(status.date).setDate(new Date(status.date).getDate() - 14));
      },
    });

    const lang = localStorage.getItem('lang');
    if (lang && lang !== '') {
      this.setLanguage(lang);
    }
  }

  setLanguage(lang: string) {
    const key = 'MENU.TOP.LANGUAGES.' + lang.toUpperCase();

    this.translate.get(key).subscribe((res: string) => {
      this.currentLanguage = res;
      this.translate.use(lang);

      localStorage.setItem('lang', lang);
    });
  }

  loadLogin() {
    import('./modules/auth/auth.module')
      .then((mod) => mod.AuthModule)
      .then((AuthModule) => {
        this.dialogService.openDialog(AuthModule.components['login'], null, null, null, false, 'full-dialog-lt-md');
      });
  }
}
