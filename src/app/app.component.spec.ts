import { TestBed, waitForAsync } from '@angular/core/testing';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { AppComponent } from './app.component';
import { AuthService } from './modules/auth/services/auth.service';
import { AppRoutingModule } from './modules/core/routing/app-routing.module';

describe('AppComponent', () => {
  const authServiceStub = {
    login: () => of(true),
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [CoreModule, MaterialModule, AppRoutingModule, TestDependenciesModule, TranslateModule.forRoot()],
      providers: [{ provide: AuthService, useValue: authServiceStub }],
    }).compileComponents();
  }));

  it('should create the app', waitForAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
