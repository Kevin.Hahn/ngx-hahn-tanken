export enum FuelType {
  E5 = 'Super E5',
  E10 = 'Super E10',
  DIESEL = 'Diesel',
}
