import { Errortype } from './errortype.enum';

export class Improvement {
  type: Errortype;
  value: string;
}
