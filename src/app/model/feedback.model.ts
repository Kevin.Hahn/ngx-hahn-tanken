export class Feedback {
  appname: string;
  kind: string;
  date: Date;
  sender: string;
  subject: string;
  name: string;
  message: string;
}
