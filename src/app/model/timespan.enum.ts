export enum TimeSpan {
  DAY = 1,
  WEEK = 7,
  TWO_WEEKS = 14,
  MONTH = 30,
}
