export class cardType {
  data: string;
  licence: string;
  ok: boolean;
  stations: Station[];
  status: string;
}

export class cardTypeDetails {
  data: string;
  licence: string;
  ok: boolean;
  station: Station;
  status: string;
}

export class Station {
  brand: string;
  diesel: number;
  dist: number;
  e5: number;
  e10: number;
  houseNumber: string;
  id: string;
  isOpen: boolean;
  lat: number;
  lng: number;
  name: string;
  place: string;
  postCode: number;
  street: string;
  price: number;

  minE5 = false;
  minE10 = false;
  minDiesel = false;

  maxE5 = false;
  maxE10 = false;
  maxDiesel = false;

  medE5 = false;
  medE10 = false;
  medDiesel = false;

  favorit = false;

  openingTimes: OpeningTimes[];
  overrides: string[];
  wholeDay: boolean;
  state: string;
}

export class OpeningTimes {
  text: string;
  start: string;
  end: string;
}

export class StationHistory {
  data: Map<Date, Station[]>;
}

export class GasStationInformationHistory {
  date: Date;
  station_uuid: string;
  diesel: number;
  e5: number;
  e10: number;
  dieselschange: boolean;
  e5change: boolean;
  e10change: boolean;
}
