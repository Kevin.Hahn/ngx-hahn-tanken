export enum HistoryDataStatus {
  FETCHING = 'Fetching',
  PREPARING = 'Preparing',
  DONE = 'Done',
}
