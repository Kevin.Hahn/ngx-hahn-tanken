interface IPriceChange {
  date: string;
  station_uuid: string;
  diesel: number;
  e5: number;
  e10: number;
  dieselchange: boolean;
  e5change: boolean;
  e10change: boolean;
}

export default IPriceChange;
