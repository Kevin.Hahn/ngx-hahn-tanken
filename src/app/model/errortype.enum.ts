export enum Errortype {
  wrongPetrolStationName = 'wrongPetrolStationName',
  wrongStatusOpen = 'wrongStatusOpen',
  wrongStatusClosed = 'wrongStatusClosed',
  wrongPriceE5 = 'wrongPriceE5',
  wrongPriceE10 = 'wrongPriceE10',
  wrongPriceDiesel = 'wrongPriceDiesel',
  wrongPetrolStationBrand = 'wrongPetrolStationBrand',
  wrongPetrolStationStreet = 'wrongPetrolStationStreet',
  wrongPetrolStationHouseNumber = 'wrongPetrolStationHouseNumber',
  wrongPetrolStationPostcode = 'wrongPetrolStationPostcode',
  wrongPetrolStationPlace = 'wrongPetrolStationPlace',
  wrongPetrolStationLocation = 'wrongPetrolStationLocation',
}
