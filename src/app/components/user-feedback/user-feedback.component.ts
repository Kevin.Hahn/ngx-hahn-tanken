import { Component, OnDestroy } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StationService } from '@core/services/station.service';
import { Feedback } from '@model/feedback.model';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

enum FeedbackType {
  Wish = 'Wish',
  Error = 'Error',
  Question = 'Question',
  Other = 'Other',
}

@Component({
  selector: 'app-user-feedback',
  templateUrl: './user-feedback.component.html',
  styleUrls: ['./user-feedback.component.scss'],
})
export class UserFeedbackComponent implements OnDestroy {
  feedbackType = FeedbackType;
  isSending = false;

  feedbackForm = new UntypedFormGroup({
    art: new UntypedFormControl('', [Validators.required]),
    name: new UntypedFormControl('', [Validators.required]),
    mail: new UntypedFormControl('', [Validators.required, Validators.email]),
    subject: new UntypedFormControl('', [Validators.required]),
    message: new UntypedFormControl('', [Validators.required]),
  });

  feedbackSub = new Subscription();

  constructor(private stationService: StationService, private snackBar: MatSnackBar, private translate: TranslateService) {}

  ngOnDestroy() {
    this.feedbackSub.unsubscribe();
  }

  sendFeedback() {
    this.feedbackForm.disable();

    const feedback = new Feedback();
    feedback.appname = 'ngx-hahn-tanken';
    feedback.date = new Date();
    feedback.kind = this.feedbackForm.get('art').value;
    feedback.name = this.feedbackForm.get('name').value;
    feedback.sender = this.feedbackForm.get('mail').value;
    feedback.subject = this.feedbackForm.get('subject').value;
    feedback.message = this.feedbackForm.get('message').value;

    this.isSending = true;

    this.feedbackSub = this.stationService.sendFeedback(feedback).subscribe({
      next: () => {
        this.translate.get('SNACKBAR.MESSAGE_OK').subscribe((res: string) => {
          this.snackBar.open(res, 'OK', { duration: 5000 });
        });
        this.feedbackForm.reset();
        this.isSending = false;
        this.feedbackForm.enable();
      },
      error: () => {
        this.translate.get('SNACKBAR.MESSAGE_ERR').subscribe((res: string) => {
          this.snackBar.open(res, 'OK', { duration: 5000 });
        });
        this.isSending = false;
        this.feedbackForm.enable();
      },
    });
  }
}
