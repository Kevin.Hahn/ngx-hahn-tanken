import { Component } from '@angular/core';
import { DisclaimerService } from '@core/services/disclaimer.service';
import { StationHistoryService } from '@core/services/station-history.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  constructor(public historyService: StationHistoryService, public disclaimerService: DisclaimerService) {}
}
