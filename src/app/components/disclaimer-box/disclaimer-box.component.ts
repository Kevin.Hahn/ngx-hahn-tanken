import { Component } from '@angular/core';
import { DisclaimerService } from '@coreService/disclaimer.service';

@Component({
  selector: 'app-disclaimer-box',
  templateUrl: './disclaimer-box.component.html',
})
export class DisclaimerBoxComponent {
  constructor(private disclaimerService: DisclaimerService) {}

  dismiss() {
    this.disclaimerService.updateDisclaimerAccepted(true);
  }
}
