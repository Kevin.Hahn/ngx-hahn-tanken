import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CoreModule } from '@core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';

import { DisclaimerBoxComponent } from './disclaimer-box.component';

/* eslint-disable @typescript-eslint/no-unused-vars */
describe('DisclaimerBoxComponent', () => {
  let component: DisclaimerBoxComponent;
  let fixture: ComponentFixture<DisclaimerBoxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DisclaimerBoxComponent],
      imports: [CoreModule, MaterialModule, TranslateModule.forRoot()],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisclaimerBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
