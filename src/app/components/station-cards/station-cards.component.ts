import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Station } from '@api/tankerkoenig/model/station';
import { fadeInOut } from '@core/animations';
import { StationService } from '@core/services/station.service';
import { CardType } from '@model/cardtype.enum';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-station-cards',
  templateUrl: './station-cards.component.html',
  styleUrls: ['./station-cards.component.scss'],
  animations: [fadeInOut],
})
export class StationCardsComponent implements OnInit, OnDestroy {
  currentFavorites = new Array<Station>();
  currentFuel: string;
  currentDate: Date;
  showStations = 8;
  cardType = CardType;
  changes: Subscription;

  constructor(public stationService: StationService, private router: Router) {}

  ngOnInit() {
    this.changes = this.stationService.changesToData.subscribe(() => {
      this.showStations = 8;
    });

    if (this.stationService.currentData) {
      this.showStations = 8;
      this.currentDate = new Date();
    } else {
      this.router.navigate(['']);
    }
  }

  ngOnDestroy() {
    this.changes.unsubscribe();
  }

  getMoreCards() {
    if (this.showStations + 8 <= this.stationService.currentData.length) {
      this.showStations += 8;
    } else {
      this.showStations = this.stationService.currentData.length;
    }
  }

  getLessCards() {
    if (this.showStations % 8 === 0 && this.showStations > 8) {
      this.showStations -= 8;
    } else {
      this.showStations -= this.showStations % 8;
    }
  }

  enableAndRouteListMode() {
    this.stationService.setCardMode(false);
    this.router.navigate(['stations/list']);
  }
}
