import { Component, Inject, OnInit } from '@angular/core';
import { MapDirectionsService } from '@angular/google-maps';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TankerkoenigSearchService } from '@api/tankerkoenig/api/api';
import { Fuel, Station } from '@api/tankerkoenig/model/models';
import { CommonService } from '@core/services/common.service';
import { StationHistoryService } from '@core/services/station-history.service';
import { StationService } from '@core/services/station.service';
import { DialogService } from '@coreService/dialog.service';
import { FuelType } from '@model/fueltype.enum';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { FeedbackComponent } from '../../feedback/feedback.component';
import { CardHistoryComponent } from '../card-history/card-history.component';

class Coordinates {
  lat: number;
  lng: number;
}

@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.scss'],
})
export class CardDetailComponent implements OnInit {
  station: Station;
  dist: number;
  detailsLoading: boolean;
  e5Data: Fuel;
  e10Data: Fuel;
  dieselData: Fuel;

  origin: Coordinates;
  destination: Coordinates;

  directionsResults$: Observable<google.maps.DirectionsResult | undefined>;

  constructor(
    @Inject(MAT_DIALOG_DATA) private cardData,
    private dialogService: DialogService,
    public historyService: StationHistoryService,
    private searchService: TankerkoenigSearchService,
    private stationService: StationService,
    public commonService: CommonService,
    private mapDirectionsService: MapDirectionsService
  ) {}

  ngOnInit() {
    const stationRef = this.cardData.data;
    this.detailsLoading = true;
    this.dist = stationRef.dist;

    this.searchService.stationsIdsGet(environment.tanken_api.API_KEY, stationRef.id).subscribe({
      next: (details) => {
        this.station = details.stations[0];
        this.detailsLoading = false;

        this.destination = new Coordinates();
        this.destination.lat = this.station.coords.lat;
        this.destination.lng = this.station.coords.lng;

        this.origin = new Coordinates();
        if (this.stationService.currentAdress) {
          this.origin.lat = this.stationService.currentAdress[0].geometry.location.lat();
          this.origin.lng = this.stationService.currentAdress[0].geometry.location.lng();
        } else {
          this.origin = this.destination;
        }

        const request: google.maps.DirectionsRequest = {
          destination: this.destination,
          origin: this.origin,
          travelMode: google.maps.TravelMode.DRIVING,
        };
        this.directionsResults$ = this.mapDirectionsService.route(request).pipe(map((response) => response.result));

        this.e5Data = this.commonService.getFuelByType(FuelType.E5, this.station);
        this.e10Data = this.commonService.getFuelByType(FuelType.E10, this.station);
        this.dieselData = this.commonService.getFuelByType(FuelType.DIESEL, this.station);
      },
    });
  }

  closeDialog() {
    this.dialogService.closeDialog();
  }

  openFeedback() {
    this.dialogService.openDialog(FeedbackComponent, this.station, null, null, false, 'feedback-dialog');
  }

  showInGoogleMaps() {
    const url = `https://www.google.com/maps/dir/?api=1&origin=${this.origin.lat},${this.origin.lng}&destination=${this.destination.lat},${this.destination.lng}`;
    window.open(url, '_blank');
  }

  loadStatistics() {
    const width = '85vw';
    this.dialogService.openDialog(CardHistoryComponent, this.station, null, width);
  }
}
