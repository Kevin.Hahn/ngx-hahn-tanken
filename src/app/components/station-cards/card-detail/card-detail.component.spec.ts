import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TankerkoenigSearchService } from '@api/tankerkoenig/api/TankerkoenigSearch.service';
import { CommonService } from '@core/services/common.service';
import { DialogService } from '@core/services/dialog.service';
import { StationService } from '@core/services/station.service';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { CardDetailComponent } from './card-detail.component';

/* eslint-disable @typescript-eslint/no-unused-vars */
describe('CardDetailComponent', () => {
  let component: CardDetailComponent;
  let fixture: ComponentFixture<CardDetailComponent>;

  const stationServiceStub = {};
  const dialogServiceStub = {};
  const commonServiceStub = {
    getFuelByType: () => {},
    getLastCharSup: () => {},
  };

  beforeEach(waitForAsync(() => {
    const dialogData = {
      data: {
        id: '12345',
        name: 'test',
        fuels: [],
        postalCode: '00001',
        coords: { lat: 0, lng: 0 },
      },
    };

    TestBed.configureTestingModule({
      declarations: [CardDetailComponent],
      imports: [TestDependenciesModule, MaterialModule],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: dialogData },
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: StationService, useValue: stationServiceStub },
        { provide: CommonService, useValue: commonServiceStub },
        TankerkoenigSearchService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
