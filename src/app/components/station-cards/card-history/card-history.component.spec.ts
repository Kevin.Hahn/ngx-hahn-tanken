import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CoreModule } from '@core/core.module';
import { ColorService } from '@core/services/color.service';
import { DialogService } from '@core/services/dialog.service';
import { StationService } from '@core/services/station.service';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { CardHistoryComponent } from './card-history.component';

describe('CardHistoryComponent', () => {
  let component: CardHistoryComponent;
  let fixture: ComponentFixture<CardHistoryComponent>;

  const stationServiceStub = {};
  const dialogServiceStub = {};
  const colorServiceStub = {};

  beforeEach(waitForAsync(() => {
    const dialogData = {
      data: {
        id: '12345',
        name: 'test',
        fuels: [],
        postalCode: '00001',
        coords: { lat: 0, lng: 0 },
      },
    };

    TestBed.configureTestingModule({
      declarations: [CardHistoryComponent],
      imports: [CoreModule, TestDependenciesModule, MaterialModule, TranslateModule.forRoot()],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: dialogData },
        { provide: StationService, useValue: stationServiceStub },
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: ColorService, useValue: colorServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
