import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Station } from '@api/tankerkoenig/model/station';
import { StationHistoryService } from '@core/services/station-history.service';
import { ColorService } from '@coreService/color.service';
import { DialogService } from '@coreService/dialog.service';
import { HistoryDataStatus } from '@model/HistoryDataStatus.enum';
import IPriceChange from '@model/pricechange.interface';
import { GasStationInformationHistory } from '@model/stations.model';
import { TimeSpan } from '@model/timespan.enum';
import { Chart, registerables } from 'chart.js';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-card-history',
  templateUrl: './card-history.component.html',
  styleUrls: ['./card-history.component.scss'],
})
export class CardHistoryComponent implements OnInit, OnDestroy {
  station: Station;
  history: GasStationInformationHistory[];
  dist: number;
  detailsLoading: boolean;
  chart: any = null;
  TimeSpan = TimeSpan;
  isLoading = false;
  step: HistoryDataStatus;
  HistoryDataStatus = HistoryDataStatus;
  updatingData = false;

  labels = new Array<string>();
  dataE5 = new Array<number>();
  dataE10 = new Array<number>();
  dataDiesel = new Array<number>();

  data: any = null;

  legend = {
    position: 'right',
  };

  options = {
    elements: {
      line: {
        tension: 0.000001,
      },
    },
  };

  historyGetSub = new Subscription();
  updateAPISub = new Subscription();
  lastUpdatedSub = new Subscription();

  constructor(@Inject(MAT_DIALOG_DATA) private cardData, private dialogService: DialogService, public historyService: StationHistoryService, private colorService: ColorService) {
    Chart.register(...registerables);
  }

  ngOnInit() {
    this.station = this.cardData.data;
  }

  ngOnDestroy() {
    this.historyGetSub.unsubscribe();
    this.updateAPISub.unsubscribe();
    this.lastUpdatedSub.unsubscribe();
  }

  loadDetails() {
    this.dialogService.goToLastDialog();
  }

  loadData(days: TimeSpan) {
    this.isLoading = true;
    this.step = HistoryDataStatus.FETCHING;
    this.historyGetSub = this.historyService.getDataForTimeSpan(days, this.station.id).subscribe({
      next: (pricechanges) => {
        this.step = HistoryDataStatus.PREPARING;
        this.preparePricechangesForDiagram(pricechanges);
        this.isLoading = false;
        this.step = HistoryDataStatus.DONE;
      },
    });
  }

  preparePricechangesForDiagram(pricechanges_days: IPriceChange[][]) {
    this.labels = new Array<string>();
    this.dataE5 = new Array<number>();
    this.dataE10 = new Array<number>();
    this.dataDiesel = new Array<number>();

    pricechanges_days.forEach((pricechanges) => {
      pricechanges.forEach((change) => {
        this.labels.push(change.date);
        this.dataE5.push(change.e5);
        this.dataE10.push(change.e10);
        this.dataDiesel.push(change.diesel);
      });
    });

    const colors = this.colorService.GetColorDemandAsSimpleArray(this.dataE5.length);

    const sharedOptions = {
      colors: colors,
      borderWidth: 1,
      fill: false,
    };

    this.data = {
      labels: this.labels,
      datasets: [
        {
          label: 'Diesel',
          data: this.dataDiesel,
          fill: sharedOptions.fill,
          backgroundColor: 'rgba(0,0,0,0.5)',
        },
        {
          label: 'E5',
          data: this.dataE5,
          fill: sharedOptions.fill,
          backgroundColor: 'rgba(89,169,150,1)',
        },
        {
          label: 'E10',
          data: this.dataE10,
          fill: sharedOptions.fill,
          backgroundColor: 'rgba(48,63,159,1)',
        },
      ],
    };
  }

  isDataOutdated() {
    const diff = Math.abs(new Date().valueOf() - new Date(this.historyService.lastStatusDate).valueOf());
    return Math.ceil(diff / (1000 * 3600 * 24)) >= 3;
  }

  refreshApiData() {
    this.updatingData = true;
    this.updateAPISub = this.historyService.updateApiData().subscribe({
      next: () => {
        this.updatingData = false;
        this.lastUpdatedSub = this.historyService.getLastUpdated().subscribe({
          next: (status) => {
            this.historyService.lastStatusDate = status.date;
            this.historyService.lastWeekDate = new Date(new Date(status.date).setDate(new Date(status.date).getDate() - 7));
            this.historyService.lastTwoWeekDate = new Date(new Date(status.date).setDate(new Date(status.date).getDate() - 14));
          },
        });
      },
    });
  }
}
