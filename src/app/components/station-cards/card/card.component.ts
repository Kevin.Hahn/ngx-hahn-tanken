import { Component, Input, OnInit } from '@angular/core';
import { Fuel, Station } from '@api/tankerkoenig/model/models';
import { CommonService } from '@core/services/common.service';
import { FavoritesService } from '@core/services/favorites.service';
import { StationService } from '@core/services/station.service';
import { DialogService } from '@coreService/dialog.service';
import { CardType } from '@model/cardtype.enum';
import { FuelType } from '@model/fueltype.enum';

import { CardDetailComponent } from '../card-detail/card-detail.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() station: Station;
  @Input() currentFuel: string = 'E5';
  @Input() type: CardType = CardType.STANDARD;
  cardType = CardType;
  e5Data: Fuel;
  e10Data: Fuel;
  dieselData: Fuel;

  constructor(public stationService: StationService, private favoriteService: FavoritesService, private dialogService: DialogService, public commonService: CommonService) {}

  ngOnInit() {
    this.e5Data = this.commonService.getFuelByType(FuelType.E5, this.station);
    this.e10Data = this.commonService.getFuelByType(FuelType.E10, this.station);
    this.dieselData = this.commonService.getFuelByType(FuelType.DIESEL, this.station);
  }

  loadDetails() {
    this.dialogService.openDialog(CardDetailComponent, this.station, null, null, false, 'detail-dialog');
  }

  headerClassByBrand(station: Station) {
    if (!station.isOpen) return 'card-header-closed';
    if (!station.brand) return 'card-header-default';

    const brand = station.brand.toLocaleLowerCase();
    return `card-header-default card-header-${brand}`;
  }

  getFavoritText(element: Station): string {
    return this.favoriteService.getFavoritText(element);
  }

  getFavoriteStarColor(element: Station): string {
    return this.favoriteService.getFavoriteStarColor(element);
  }

  setFavorite(station: Station) {
    this.favoriteService.setFavorite(station);
  }

  deleteFromFavorites(station: Station) {
    this.favoriteService.deleteFromFavorites(station);
  }

  getStationNearClass(dist: number): string {
    return this.stationService.getStationNearClass(dist);
  }

  getStationNearTooltip(dist: number): string {
    return this.stationService.getStationNearTooltip(dist);
  }
}
