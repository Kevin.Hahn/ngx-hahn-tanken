import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Station } from '@api/tankerkoenig/model/station';
import { CoreModule } from '@core/core.module';
import { CommonService } from '@core/services/common.service';
import { DialogService } from '@core/services/dialog.service';
import { CardType } from '@model/cardtype.enum';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { CardComponent } from './card.component';

/* eslint-disable @typescript-eslint/no-unused-vars */
describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<TestComponentWrapper>;

  const dialogServiceStub = {};
  const commonServiceStub = {
    getFuelByType: () => {},
    getLastCharSup: () => {},
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CardComponent, TestComponentWrapper],
      imports: [CoreModule, TestDependenciesModule, MaterialModule, TranslateModule.forRoot()],
      providers: [
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: CommonService, useValue: commonServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponentWrapper);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: 'test-component-wrapper',
  template: '<app-card [station]="station" [fuel]="fuel" [type]="type"></app-card>',
})
class TestComponentWrapper {
  station: Station = {
    id: '12345',
    name: 'test',
    fuels: [],
    postalCode: '00001',
    coords: { lat: 0, lng: 0 },
    isOpen: true,
  };

  fuel: string = 'E5';
  type: CardType = CardType.STANDARD;
}
