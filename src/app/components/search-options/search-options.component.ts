import { Component, Input, ViewChild } from '@angular/core';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from '@coreService/dialog.service';
import { SearchService } from '@coreService/search.service';
import { TranslateService } from '@ngx-translate/core';

import { SearchInputService } from '../../modules/core/services/search-input.service';

@Component({
  selector: 'app-search-options',
  templateUrl: './search-options.component.html',
  styleUrls: ['./search-options.component.scss'],
})
export class SearchOptionsComponent {
  @ViewChild('radius', { static: true }) radius: MatSelect;
  @ViewChild('fuel', { static: true }) fuel: MatSelect;
  @ViewChild('lowestPrice', { static: true }) lowestPrice: MatCheckbox;
  @ViewChild('stationfilter', { static: true }) stationfilter: MatSelect;
  @Input() adress: HTMLInputElement;

  constructor(
    private dialogService: DialogService,
    public searchInputService: SearchInputService,
    private searchService: SearchService,
    private snackBar: MatSnackBar,
    private translate: TranslateService
  ) {}

  setChosenOptions() {
    this.searchInputService.fuel = this.fuel.value;
    this.searchInputService.radius = this.radius.value;
    this.searchInputService.lowestPrice = this.lowestPrice.checked;
    this.searchInputService.stationFilter = this.stationfilter.value;
  }

  closeOptionDialog(search = true) {
    if (search) {
      this.setChosenOptions();
    }

    this.dialogService.closeDialog(search);
  }

  closeDropdown() {
    this.setChosenOptions();
    if (this.adress.value === '') {
      this.translate.get('SNACKBAR.ENTERADRESS').subscribe((res: string) => {
        this.snackBar.open(res, 'OK', { duration: 5000 });
      });
    } else {
      this.searchService.startSearch(this.adress.value);
    }
  }
}
