import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DialogService } from '@core/services/dialog.service';
import { SearchService } from '@core/services/search.service';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { SearchOptionsComponent } from './search-options.component';

describe('SearchOptionsComponent', () => {
  let component: SearchOptionsComponent;
  let fixture: ComponentFixture<SearchOptionsComponent>;

  const dialogServiceStub = {};
  const searchServiceStub = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SearchOptionsComponent],
      imports: [TestDependenciesModule, MaterialModule, TranslateModule.forRoot()],
      providers: [
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: SearchService, useValue: searchServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
