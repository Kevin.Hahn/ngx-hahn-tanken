import { Component, Input, OnInit } from '@angular/core';
import { Fuel, Station } from '@api/tankerkoenig/model/models';
import { CommonService } from '@core/services/common.service';
import { DialogService } from '@coreService/dialog.service';
import { FuelType } from '@model/fueltype.enum';

import { CardDetailComponent } from '../station-cards/card-detail/card-detail.component';

@Component({
  selector: 'app-favoriten-pin',
  templateUrl: './favoriten-pin.component.html',

  styleUrls: ['./favoriten-pin.component.scss'],
})
export class FavoritenPinComponent implements OnInit {
  @Input() favorit: Station;
  e5Data: Fuel;
  e10Data: Fuel;
  dieselData: Fuel;

  constructor(private dialogService: DialogService, public commonService: CommonService) {}

  ngOnInit() {
    this.e5Data = this.commonService.getFuelByType(FuelType.E5, this.favorit);
    this.e10Data = this.commonService.getFuelByType(FuelType.E10, this.favorit);
    this.dieselData = this.commonService.getFuelByType(FuelType.DIESEL, this.favorit);
  }

  loadDetails() {
    this.dialogService.openDialog(CardDetailComponent, this.favorit, null, null, false, 'detail-dialog');
  }
}
