/* eslint-disable @typescript-eslint/no-unused-vars */
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Station } from '@api/tankerkoenig/model/models';
import { CommonService } from '@core/services/common.service';
import { DialogService } from '@core/services/dialog.service';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { FavoritenPinComponent } from './favoriten-pin.component';

describe('FavoritenPinComponent', () => {
  let component: FavoritenPinComponent;
  let fixture: ComponentFixture<TestComponentWrapper>;

  const dialogServiceStub = {};
  const commonServiceStub = {
    getFuelByType: () => {},
    getLastCharSup: () => {},
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FavoritenPinComponent, TestComponentWrapper],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [TestDependenciesModule],
      providers: [
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: CommonService, useValue: commonServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponentWrapper);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: 'test-component-wrapper',
  template: '<app-favoriten-pin [favorit]="favorit"></app-favoriten-pin>',
})
class TestComponentWrapper {
  favorit: Station = {
    id: '12345',
    name: 'test',
    fuels: [],
    postalCode: '00001',
    coords: { lat: 0, lng: 0 },
  };
}
