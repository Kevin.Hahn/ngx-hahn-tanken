import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TankerkoenigSearchService } from '@api/tankerkoenig/api/TankerkoenigSearch.service';
import { TranslateModule } from '@ngx-translate/core';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { FavoritesMapComponent } from './favorite-map.component';

describe('FavoritesMapComponent', () => {
  let component: FavoritesMapComponent;
  let fixture: ComponentFixture<FavoritesMapComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestDependenciesModule, TranslateModule.forRoot()],
      declarations: [FavoritesMapComponent],
      providers: [TankerkoenigSearchService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritesMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
