import { Component, OnInit, ViewChild } from '@angular/core';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';
import { TankerkoenigSearchService } from '@api/tankerkoenig/api/api';
import { Station, Stations } from '@api/tankerkoenig/model/models';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-favorite-map',
  templateUrl: './favorite-map.component.html',
  styleUrls: ['./favorite-map.component.scss'],
})
export class FavoritesMapComponent implements OnInit {
  favorites: Stations;
  hasFavorites: boolean;
  currentPosition: any;
  firstFavorit: Station;
  @ViewChild(MapInfoWindow) infoWindow: MapInfoWindow;

  constructor(private searchService: TankerkoenigSearchService) {}

  ngOnInit() {
    const favorites = localStorage.getItem('favorites');
    if (favorites && favorites !== '') {
      this.hasFavorites = true;
      this.prepareFavorites(localStorage.getItem('favorites').split(';'));
    } else {
      this.hasFavorites = false;
    }
  }

  prepareFavorites(favoritenIds: string[]) {
    this.searchService.stationsIdsGet(environment.tanken_api.API_KEY, favoritenIds.join(',')).subscribe({
      next: (favs) => {
        this.favorites = favs.stations;
        this.firstFavorit = this.favorites[0];
      },
    });
  }

  openInfoWindow(marker: MapMarker) {
    this.infoWindow.open(marker);
  }
}
