import { Component, OnInit } from '@angular/core';
import { TankerkoenigStatisticsService } from '@api/tankerkoenig/api/api';
import { ResponseStats200 } from '@api/tankerkoenig/model/models';
import { CommonService } from '@core/services/common.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
})
export class StatsComponent implements OnInit {
  stats: ResponseStats200;

  constructor(private statsService: TankerkoenigStatisticsService, public commonService: CommonService) {}

  ngOnInit() {
    this.statsService.statsGet(environment.tanken_api.API_KEY_STATS).subscribe({
      next: (stats) => {
        this.stats = stats;
      },
    });
  }
}
