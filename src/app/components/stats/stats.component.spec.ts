/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TankerkoenigStatisticsService } from '@api/tankerkoenig/api/TankerkoenigStatistics.service';
import { CoreModule } from '@core/core.module';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { StatsComponent } from './stats.component';

describe('StatsComponent', () => {
  let component: StatsComponent;
  let fixture: ComponentFixture<StatsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, TestDependenciesModule],
      declarations: [StatsComponent],
      providers: [TankerkoenigStatisticsService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
