import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogService } from '@core/services/dialog.service';
import { StationService } from '@core/services/station.service';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { FeedbackComponent } from './feedback.component';

describe('FeedbackComponent', () => {
  let component: FeedbackComponent;
  let fixture: ComponentFixture<FeedbackComponent>;

  const stationServiceStub = {};
  const dialogServiceStub = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FeedbackComponent],
      imports: [TestDependenciesModule, MaterialModule, TranslateModule.forRoot()],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: StationService, useValue: stationServiceStub },
        { provide: DialogService, useValue: dialogServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
