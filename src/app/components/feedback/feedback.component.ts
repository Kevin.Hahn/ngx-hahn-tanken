import { Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Station } from '@api/tankerkoenig/model/models';
import { StationService } from '@core/services/station.service';
import { DialogService } from '@coreService/dialog.service';
import { Errortype } from '@model/errortype.enum';
import { Improvement } from '@model/improvement.model';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit, OnDestroy {
  station: Station;
  selectedImprovement: Improvement;
  errortype = Errortype;
  @ViewChild('improvement', { static: false }) improvement: ElementRef;
  @ViewChild('lat', { static: true }) lat: ElementRef;
  @ViewChild('lng', { static: true }) lng: ElementRef;

  improvements: Improvement[] = [
    { type: Errortype.wrongPetrolStationName, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPetrolStationName' },
    { type: Errortype.wrongStatusOpen, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongStatusOpen' },
    // { type: Errortype.wrongStatusClosed, value: 'Geschlossen melden' },
    { type: Errortype.wrongPriceE5, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPriceE5' },
    { type: Errortype.wrongPriceE10, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPriceE10' },
    { type: Errortype.wrongPriceDiesel, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPriceDiesel' },
    { type: Errortype.wrongPetrolStationBrand, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPetrolStationBrand' },
    { type: Errortype.wrongPetrolStationStreet, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPetrolStationStreet' },
    { type: Errortype.wrongPetrolStationHouseNumber, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPetrolStationHouseNumber' },
    { type: Errortype.wrongPetrolStationPostcode, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPetrolStationPostcode' },
    { type: Errortype.wrongPetrolStationPlace, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPetrolStationPlace' },
    { type: Errortype.wrongPetrolStationLocation, value: 'COMPONENTS.FEEDBACK.IMPROVEMENTS.wrongPetrolStationLocation' },
  ];

  feedback = new Subscription();

  constructor(
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) private stationData,
    private dialogService: DialogService,
    private stationService: StationService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.station = this.stationData.data;
  }

  ngOnDestroy() {
    this.feedback.unsubscribe();
  }

  backToStationDetails() {
    this.dialogService.goToLastDialog();
  }

  setImprovements(chosenImprovements: Improvement) {
    this.selectedImprovement = chosenImprovements;
  }

  sendFeedback() {
    this.feedback = this.stationService.sendComplaint(this.station, this.selectedImprovement, this.improvement, this.lat, this.lng).subscribe({
      next: () => {
        this.translate.get('SNACKBAR.FEEDBACK').subscribe((res: string) => {
          this.snackBar.open(res, 'OK', { duration: 5000 });
        });
        this.selectedImprovement = null;
      },
    });
  }
}
