import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StationService } from '@core/services/station.service';
import { DialogService } from '@coreService/dialog.service';
import { SearchService } from '@coreService/search.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

import { SearchOptionsComponent } from '../search-options/search-options.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @ViewChild('adress', { static: true }) adress: ElementRef;

  constructor(public stationService: StationService, private dialogService: DialogService, private searchService: SearchService, private snackBar: MatSnackBar, private translate: TranslateService) {}

  ngOnInit() {
    if (!environment.production) {
      const adress = environment.adress;
      this.adress.nativeElement.value = adress;
    }
  }

  openOptionen() {
    this.dialogService.openDialog(SearchOptionsComponent, null, null, null, true, 'full-dialog-lt-md');
    this.dialogService.onClose().subscribe((suchen) => {
      if (suchen) {
        this.searchService.startSearch(this.adress.nativeElement.value);
      }
    });
  }

  async startSearch(adresse: string) {
    return this.searchService.startSearch(adresse);
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (location) => {
          this.adress.nativeElement.value = location.coords.latitude + ',' + location.coords.longitude;
        },
        () => {
          if (location.protocol !== 'https:') {
            this.translate.get('SNACKBAR.NOSSL').subscribe((res: string) => {
              this.snackBar.open(res, 'OK', { duration: 5000 });
            });
          } else {
            this.translate.get('SNACKBAR.BROWSER').subscribe((res: string) => {
              this.snackBar.open(res, 'OK', { duration: 5000 });
            });
          }
        }
      );
    }
  }
}
