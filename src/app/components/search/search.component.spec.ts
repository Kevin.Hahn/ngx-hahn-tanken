import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DialogService } from '@core/services/dialog.service';
import { SearchService } from '@core/services/search.service';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/modules/shared/material/material.module';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { SearchComponent } from './search.component';

/* eslint-disable @typescript-eslint/no-unused-vars */
describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  const dialogServiceStub = {};
  const searchServiceStub = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent],
      imports: [TestDependenciesModule, MaterialModule, TranslateModule.forRoot()],
      providers: [
        { provide: DialogService, useValue: dialogServiceStub },
        { provide: SearchService, useValue: searchServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
