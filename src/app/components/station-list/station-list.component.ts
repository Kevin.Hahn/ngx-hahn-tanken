import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { fadeInOut } from '@core/animations';
import { StationService } from '@core/services/station.service';

@Component({
  selector: 'app-station-list',
  templateUrl: './station-list.component.html',
  styleUrls: ['./station-list.component.scss'],
  animations: [fadeInOut],
})
export class StationListComponent {
  constructor(public stationService: StationService, private router: Router) {}

  enableAndRouteCardMode() {
    this.stationService.setCardMode(true);
    this.router.navigate(['stations/cards']);
  }
}
