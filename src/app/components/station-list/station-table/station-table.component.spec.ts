import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CoreModule } from '@core/core.module';
import { DialogService } from '@core/services/dialog.service';
import { TranslateModule } from '@ngx-translate/core';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { StationTableComponent } from './station-table.component';

describe('StationTableComponent', () => {
  let component: StationTableComponent;
  let fixture: ComponentFixture<StationTableComponent>;

  const dialogServiceStub = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestDependenciesModule, CoreModule, TranslateModule.forRoot()],
      declarations: [StationTableComponent],
      providers: [{ provide: DialogService, useValue: dialogServiceStub }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
