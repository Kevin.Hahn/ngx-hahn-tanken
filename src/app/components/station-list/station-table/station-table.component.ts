import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Station } from '@api/tankerkoenig/model/models';
import { CommonService } from '@core/services/common.service';
import { FavoritesService } from '@core/services/favorites.service';
import { SearchInputService } from '@core/services/search-input.service';
import { StationService } from '@core/services/station.service';
import { DialogService } from '@coreService/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { CardDetailComponent } from '../../station-cards/card-detail/card-detail.component';

@Component({
  selector: 'app-station-table',
  templateUrl: './station-table.component.html',
  styleUrls: ['./station-table.component.scss'],
})
export class StationTableComponent implements OnInit, OnDestroy {
  dataSource: MatTableDataSource<Station>;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() tableName: string;

  currentFuel: string;
  currentDate: Date;
  flexMediaWatcher: Subscription;
  currentScreenWidth = '';
  cols: string[];
  tableHidden = false;

  changes: Subscription;

  @ViewChild('sort', { static: true }) sort: MatSort;
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;

  constructor(
    private dialogService: DialogService,
    private snackBar: MatSnackBar,
    public stationService: StationService,
    private translate: TranslateService,
    public searchInputService: SearchInputService,
    public commonService: CommonService,
    private favoriteService: FavoritesService,
    mediaObserver: MediaObserver
  ) {
    this.flexMediaWatcher = mediaObserver.asObservable().subscribe({
      next: (change: MediaChange[]) => {
        if (change[0].mqAlias !== this.currentScreenWidth) {
          this.currentScreenWidth = change[0].mqAlias;
          this.setupTable();
        }
      },
    });
  }

  ngOnInit() {
    this.refresh();

    this.changes = this.stationService.changesToData.subscribe({
      next: () => {
        this.refresh();
      },
    });
  }

  ngOnDestroy() {
    this.flexMediaWatcher.unsubscribe();
    this.changes.unsubscribe();
  }

  refresh() {
    let stations: Station[];
    switch (this.tableName) {
      case 'overview':
        stations = this.stationService.currentData;
        break;
      case 'favorites':
        stations = this.stationService.currentFavorites;
        break;
      case 'hidden':
        stations = this.stationService.currentHiddenStations;
        break;
    }

    if (stations && stations.length > 0) {
      this.dataSource = this.stationService.prepareData(stations, this.sort, this.paginator);
      this.tableHidden = false;
    } else {
      this.dataSource = new MatTableDataSource<Station>();
      this.tableHidden = true;
    }
  }

  setupTable() {
    this.cols = ['name', 'dist', 'e5', 'e10', 'diesel', 'actions_normal'];
    if (this.currentScreenWidth === 'xs' || this.currentScreenWidth === 'sm') {
      this.cols = ['name', 'dist', 'e5', 'e10', 'diesel', 'actions_mobile'];
    }
  }

  getStationNearClass(dist: number): string {
    return this.stationService.getStationNearClass(dist);
  }

  getStationNearTooltip(dist: number): string {
    return this.stationService.getStationNearTooltip(dist);
  }

  loadDetails(station: Station) {
    this.dialogService.openDialog(CardDetailComponent, station, null, null, false, 'detail-dialog');
  }

  getFavoritText(element: Station): string {
    return this.favoriteService.getFavoritText(element);
  }

  getFavoriteStarColor(element: Station): string {
    return this.favoriteService.getFavoriteStarColor(element);
  }

  setFavorite(station: Station) {
    this.favoriteService.setFavorite(station);
  }

  deleteFromFavorites(station: Station) {
    this.favoriteService.deleteFromFavorites(station);
  }

  hideStation(station: Station) {
    this.stationService.hideStation(station);
    this.stationService.changesToData.next(null);
    this.translate.get('SNACKBAR.STATION_HIDE').subscribe((res: string) => {
      this.snackBar.open(res, 'OK', { duration: 5000 });
    });
  }

  showStation(station: Station) {
    this.stationService.showStation(station);
    this.stationService.changesToData.next(null);
    this.translate.get('SNACKBAR.STATION_SHOW').subscribe((res: string) => {
      this.snackBar.open(res, 'OK', { duration: 5000 });
    });
  }
}
