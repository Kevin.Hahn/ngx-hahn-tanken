import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DependenciesModule } from '@core/dependencies.module';
import { TranslateModule } from '@ngx-translate/core';

import { UserDashboardComponent } from './components/user-dashboard/user-dashboard.component';
import { UserMenuComponent } from './components/user-menu/user-menu.component';
import { userRoutes } from './user.routes';

const components = [UserDashboardComponent, UserMenuComponent];

@NgModule({
  imports: [DependenciesModule, TranslateModule.forChild({}), RouterModule.forChild(userRoutes)],
  declarations: [...components],
  exports: [RouterModule, ...components],
})
export class UserModule {
  static components = {
    usermenu: UserMenuComponent,
    dashboard: UserDashboardComponent,
  };
}
