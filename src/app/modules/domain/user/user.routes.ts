import { Routes } from '@angular/router';

import { UserDashboardComponent } from './components/user-dashboard/user-dashboard.component';

export const userRoutes: Routes = [
  {
    path: '',
    children: [{ path: 'dashboard', component: UserDashboardComponent }],
  },
];
