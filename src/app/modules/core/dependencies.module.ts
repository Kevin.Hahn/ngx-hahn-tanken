import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HAMMER_GESTURE_CONFIG, HammerGestureConfig, HammerModule } from '@angular/platform-browser';

import { MaterialModule } from '../shared/material/material.module';
import { CoreModule } from './core.module';

@Injectable()
export class HammerConfig extends HammerGestureConfig {
  overrides = <any>{
    // pinch: { enable: false },
    // rotate: { enable: false },
  };
}

@NgModule({
  imports: [],
  exports: [HammerModule, CommonModule, FormsModule, ReactiveFormsModule, HttpClientModule, MaterialModule, FlexLayoutModule, CoreModule],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig,
    },
  ],
})
export class DependenciesModule {}
