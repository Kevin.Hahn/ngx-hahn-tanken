import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../shared/material/material.module';
import { ApiDateFormatPipe, EnumToArrayPipe, ReplaceEmptyPipe, SafePipe } from './pipes';
import { ColorService } from './services/color.service';
import { CommonService } from './services/common.service';
import { DialogService } from './services/dialog.service';
import { DisclaimerService } from './services/disclaimer.service';
import { FavoritesService } from './services/favorites.service';
import { GeocodingService } from './services/geocoding.service';
import { SearchInputService } from './services/search-input.service';
import { SearchService } from './services/search.service';
import { StationService } from './services/station.service';

@NgModule({
  imports: [CommonModule, MaterialModule, FlexLayoutModule],
  declarations: [ReplaceEmptyPipe, EnumToArrayPipe, ApiDateFormatPipe, SafePipe],
  exports: [MaterialModule, FlexLayoutModule, ReplaceEmptyPipe, EnumToArrayPipe, ApiDateFormatPipe, SafePipe],
  providers: [SearchService, StationService, GeocodingService, DialogService, SearchInputService, DisclaimerService, ColorService, CommonService, FavoritesService],
})
export class CoreModule {}
