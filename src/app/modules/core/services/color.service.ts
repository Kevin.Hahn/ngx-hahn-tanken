import { Injectable } from '@angular/core';

/** Farbmodel */
export class Color {
  /** Ausprägung der Farbe in der Palette */
  Variant: string;
  /** HexWert */
  ColorHex: string;
  /** Muss die Schrift weiß erzwungen werden? */
  WhiteColor: boolean;
}

/** Service für Farben */
@Injectable()
export class ColorService {
  /** Beziehen der Primärfarbe */
  // public primaryColor = this.rgb2hex(window.getComputedStyle(document.querySelector('#diagramm-primary')).getPropertyValue('color'));
  /** Beziehen der Sekundärfarbe */
  // public accentColor = this.rgb2hex(window.getComputedStyle(document.querySelector('#diagramm-accent')).getPropertyValue('color'));
  /** Beziehen der Warnfarbe */
  // public warnColor = this.rgb2hex(window.getComputedStyle(document.querySelector('#diagramm-warn')).getPropertyValue('color'));

  /**
   * Versucht anhand eines Hex-Wertes in den Colorpalettes die Entsprechung zu finden und darüber die korrekte Palette zu determinieren
   * @param ColorHex Farbwert in Hex mit #
   * @returns Palette
   */
  FindColorPalette(ColorHex: string): Color[] {
    let ColorPalette = null;

    this.Palettes.forEach((colorPalette) => {
      for (const element of colorPalette) {
        if (element.ColorHex.toUpperCase() === ColorHex.toUpperCase()) {
          ColorPalette = colorPalette;
          break;
        }
      }
    });

    return ColorPalette;
  }

  /**
   * Versucht anhand eines Hex-Wertes in den Colorpalettes die Entsprechung zu finden
   * @param Palette Material Farbpalette
   * @param [SkipAColors=false] SkipAColors Sollen die Farbwerte mit A in der Variante übersprungen werden?
   * @returns Hex-Farben als String[]
   */
  GetColorPaletteAsSimpleArray(Palette: Color[], SkipAColors = false): string[] {
    const colorsArray: string[] = [];

    // Fallback auf Blue
    if (Palette === null) {
      Palette = this.Blue;
    }

    for (const element of Palette) {
      if (element.Variant.indexOf('A') !== -1 && SkipAColors) {
        continue;
      }

      colorsArray.push(element.ColorHex);
    }

    return colorsArray;
  }

  /**
   * Bezieht anhand der Anzahl der gewünschten Farben eine bunte Mischung an Farben aus allen Material Palettes von vorn an
   * @param numberOfColors Anzahl der gewünschten Farben
   * @param [SkipAColors=false] SkipAColors Sollen die Farbwerte mit A in der Variante übersprungen werden?
   */
  GetColorDemandAsSimpleArray(numberOfColors: number, skipAColors = false): string[] {
    const colorsArray: string[] = [];

    let currentPaletteIndex = 0;
    let currentPalettePositionIndex = 0;
    do {
      const palette = this.Palettes[currentPaletteIndex];
      const color = palette[currentPalettePositionIndex];

      if (!color.Variant.startsWith('A') && !skipAColors) {
        colorsArray.push(color.ColorHex);
      }

      if (currentPaletteIndex < this.Palettes.length - 1) {
        currentPaletteIndex += 1;
      } else {
        currentPaletteIndex = 0;
      }
    } while (colorsArray.length !== numberOfColors);

    return colorsArray;
  }

  /**
   * Versucht anhand eines Hex-Wertes in den Colorpalettes die Color Entsprechung zu finden
   * @param ColorHex Hex Farbwert mit #
   * @returns Color Objekt oder null
   */
  GetColorObjectFromPalettes(ColorHex: string): Color {
    let ColorObject = new Color();
    if (ColorHex !== undefined) {
      this.Palettes.forEach((colorPalette) => {
        for (const element of colorPalette) {
          if (element.ColorHex.toUpperCase() === ColorHex.toUpperCase()) {
            ColorObject = element;
            break;
          }
        }
      });
    }
    return ColorObject;
  }

  /**
   * Gibt den gewünschten Farbwert aus der ZielPalette zurück
   * @param ColorPalette Name der Palette
   * @param [Variant='500'] Variante der Farbe
   * @returns Hex-Farbwert der Entsprechung
   */
  GetColor(ColorPalette: string, Variant = '500'): string {
    switch (ColorPalette.toLocaleUpperCase()) {
      case 'RED':
        return this.GetColorFromPalette(this.Red, Variant);
      case 'PINK':
        return this.GetColorFromPalette(this.Pink, Variant);
      case 'PURPLE':
        return this.GetColorFromPalette(this.Purple, Variant);
      case 'DEEP PURPLE':
      case 'DEEPPURPLE':
        return this.GetColorFromPalette(this.DeepPurple, Variant);
      case 'INDIGO':
        return this.GetColorFromPalette(this.Indigo, Variant);
      case 'BLUE':
        return this.GetColorFromPalette(this.Blue, Variant);
      case 'LIGHT BLUE':
      case 'LIGHTBLUE':
        return this.GetColorFromPalette(this.LightBlue, Variant);
      case 'CYAN':
        return this.GetColorFromPalette(this.Cyan, Variant);
      case 'GREEN':
        return this.GetColorFromPalette(this.Green, Variant);
      case 'LIGHT GREEN':
      case 'LIGHTGREEN':
        return this.GetColorFromPalette(this.LightGreen, Variant);
      case 'LIME':
        return this.GetColorFromPalette(this.Lime, Variant);
      case 'YELLOW':
        return this.GetColorFromPalette(this.Yellow, Variant);
      case 'AMBER':
        return this.GetColorFromPalette(this.Amber, Variant);
      case 'ORANGE':
        return this.GetColorFromPalette(this.Orange, Variant);
      case 'DEEP ORANGE':
      case 'DEEPORANGE':
        return this.GetColorFromPalette(this.DeepOrange, Variant);
      case 'BROWN':
        return this.GetColorFromPalette(this.Brown, Variant);
      case 'GREY':
        return this.GetColorFromPalette(this.Grey, Variant);
      case 'BLUE GREY':
      case 'BLUEGREY':
      default:
        return this.GetColorFromPalette(this.BlueGrey, Variant);
    }
  }

  /**
   * Gibt den gewünschten Farbwert aus der ZielPalette zurück
   * @param ColorPalette Palette
   * @param Variant Variante der Farbe
   * @returns Hex-Farbwert der Entsprechung
   */
  private GetColorFromPalette(ColorPalette: Array<Color>, Variant: string): string {
    return ColorPalette[Variant].value;
  }

  /**
   * Stuft einen Hex-Farbwert gegen eine Helligkeit ab
   * @param hex Hex-Farbwert
   * @param lum Helligkeit der Farbe als Zahl 0-1
   * @returns Hex-Farbwert
   */
  ColorLuminance(hex: string, lum: number): string {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    let rgb = '#',
      c,
      i;
    for (i = 0; i < 3; i++) {
      c = parseInt(hex.substring(i * 2, 2), 16);
      c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16);
      rgb += ('00' + c).substring(c.length);
    }

    return rgb;
  }

  /**
   * Hilfsmethode von rgb2hex
   * @returns Hex-Farbwert der Entsprechung
   */
  hex(x): string {
    const hexDigits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

    return isNaN(x) ? '00' : hexDigits[(x - (x % 16)) / 16] + hexDigits[x % 16];
  }

  /**
   * Konvertiert einen rgb-Farbwert in einen Hex-Farbwert
   * @param rgb rgb-Farbwert
   * @returns Hex-Farbwert
   */
  rgb2hex(rgb): string {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return '#' + this.hex(rgb[1]) + this.hex(rgb[2]) + this.hex(rgb[3]);
  }

  // https://material.io/guidelines/style/color.html#color-color-palette

  /** Material: Red */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Red: Color[] = [
    { Variant: '50', ColorHex: '#FFEBEE', WhiteColor: false },
    { Variant: '100', ColorHex: '#FFCDD2', WhiteColor: false },
    { Variant: '200', ColorHex: '#EF9A9A', WhiteColor: false },
    { Variant: '300', ColorHex: '#E57373', WhiteColor: false },
    { Variant: '400', ColorHex: '#EF5350', WhiteColor: true },
    { Variant: '500', ColorHex: '#F44336', WhiteColor: true },
    { Variant: '600', ColorHex: '#E53935', WhiteColor: true },
    { Variant: '700', ColorHex: '#D32F2F', WhiteColor: true },
    { Variant: '800', ColorHex: '#C62828', WhiteColor: true },
    { Variant: '900', ColorHex: '#B71C1C', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#FF8A80', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#FF5252', WhiteColor: true },
    { Variant: 'A400', ColorHex: '#FF1744', WhiteColor: true },
    { Variant: 'A700', ColorHex: '#D50000', WhiteColor: true },
  ];

  /** Material: Pink */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Pink: Color[] = [
    { Variant: '50', ColorHex: '#FCE4EC', WhiteColor: false },
    { Variant: '100', ColorHex: '#F8BBD0', WhiteColor: false },
    { Variant: '200', ColorHex: '#F48FB1', WhiteColor: false },
    { Variant: '300', ColorHex: '#F06292', WhiteColor: false },
    { Variant: '400', ColorHex: '#EC407A', WhiteColor: true },
    { Variant: '500', ColorHex: '#E91E63', WhiteColor: true },
    { Variant: '600', ColorHex: '#D81B60', WhiteColor: true },
    { Variant: '700', ColorHex: '#C2185B', WhiteColor: true },
    { Variant: '800', ColorHex: '#AD1457', WhiteColor: true },
    { Variant: '900', ColorHex: '#880E4F', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#FF80AB', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#FF4081', WhiteColor: true },
    { Variant: 'A400', ColorHex: '#F50057', WhiteColor: true },
    { Variant: 'A700', ColorHex: '#C51162', WhiteColor: true },
  ];

  /** Material: Purple */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Purple: Color[] = [
    { Variant: '50', ColorHex: '#F3E5F5', WhiteColor: false },
    { Variant: '100', ColorHex: '#E1BEE7', WhiteColor: false },
    { Variant: '200', ColorHex: '#CE93D8', WhiteColor: false },
    { Variant: '300', ColorHex: '#BA68C8', WhiteColor: true },
    { Variant: '400', ColorHex: '#AB47BC', WhiteColor: true },
    { Variant: '500', ColorHex: '#9C27B0', WhiteColor: true },
    { Variant: '600', ColorHex: '#8E24AA', WhiteColor: true },
    { Variant: '700', ColorHex: '#7B1FA2', WhiteColor: true },
    { Variant: '800', ColorHex: '#6A1B9A', WhiteColor: true },
    { Variant: '900', ColorHex: '#4A148C', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#EA80FC', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#E040FB', WhiteColor: true },
    { Variant: 'A400', ColorHex: '#D500F9', WhiteColor: true },
    { Variant: 'A700', ColorHex: '#AA00FF', WhiteColor: true },
  ];

  /** Material: DeepPurple */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public DeepPurple: Color[] = [
    { Variant: '50', ColorHex: '#EDE7F6', WhiteColor: false },
    { Variant: '100', ColorHex: '#D1C4E9', WhiteColor: false },
    { Variant: '200', ColorHex: '#B39DDB', WhiteColor: false },
    { Variant: '300', ColorHex: '#9575CD', WhiteColor: true },
    { Variant: '400', ColorHex: '#7E57C2', WhiteColor: true },
    { Variant: '500', ColorHex: '#673AB7', WhiteColor: true },
    { Variant: '600', ColorHex: '#5E35B1', WhiteColor: true },
    { Variant: '700', ColorHex: '#512DA8', WhiteColor: true },
    { Variant: '800', ColorHex: '#4527A0', WhiteColor: true },
    { Variant: '900', ColorHex: '#311B92', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#B388FF', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#7C4DFF', WhiteColor: true },
    { Variant: 'A400', ColorHex: '#651FFF', WhiteColor: true },
    { Variant: 'A700', ColorHex: '#6200EA', WhiteColor: true },
  ];

  /** Material: Indigo */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Indigo: Color[] = [
    { Variant: '50', ColorHex: '#E8EAF6', WhiteColor: false },
    { Variant: '100', ColorHex: '#C5CAE9', WhiteColor: false },
    { Variant: '200', ColorHex: '#9FA8DA', WhiteColor: false },
    { Variant: '300', ColorHex: '#7986CB', WhiteColor: true },
    { Variant: '400', ColorHex: '#5C6BC0', WhiteColor: true },
    { Variant: '500', ColorHex: '#3F51B5', WhiteColor: true },
    { Variant: '600', ColorHex: '#3949AB', WhiteColor: true },
    { Variant: '700', ColorHex: '#303F9F', WhiteColor: true },
    { Variant: '800', ColorHex: '#283593', WhiteColor: true },
    { Variant: '900', ColorHex: '#1A237E', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#8C9EFF', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#536DFE', WhiteColor: true },
    { Variant: 'A400', ColorHex: '#3D5AFE', WhiteColor: true },
    { Variant: 'A700', ColorHex: '#304FFE', WhiteColor: true },
  ];

  /** Material: Blue */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Blue: Color[] = [
    { Variant: '50', ColorHex: '#E3F2FD', WhiteColor: false },
    { Variant: '100', ColorHex: '#BBDEFB', WhiteColor: false },
    { Variant: '200', ColorHex: '#90CAF9', WhiteColor: false },
    { Variant: '300', ColorHex: '#64B5F6', WhiteColor: false },
    { Variant: '400', ColorHex: '#42A5F5', WhiteColor: false },
    { Variant: '500', ColorHex: '#2196F3', WhiteColor: false },
    { Variant: '600', ColorHex: '#1E88E5', WhiteColor: true },
    { Variant: '700', ColorHex: '#1976D2', WhiteColor: true },
    { Variant: '800', ColorHex: '#1565C0', WhiteColor: true },
    { Variant: '900', ColorHex: '#0D47A1', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#82B1FF', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#448AFF', WhiteColor: true },
    { Variant: 'A400', ColorHex: '#2979FF', WhiteColor: true },
    { Variant: 'A700', ColorHex: '#2962FF', WhiteColor: true },
  ];

  /** Material: LightBlue */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public LightBlue: Color[] = [
    { Variant: '50', ColorHex: '#E1F5FE', WhiteColor: false },
    { Variant: '100', ColorHex: '#B3E5FC', WhiteColor: false },
    { Variant: '200', ColorHex: '#81D4FA', WhiteColor: false },
    { Variant: '300', ColorHex: '#4FC3F7', WhiteColor: false },
    { Variant: '400', ColorHex: '#29B6F6', WhiteColor: false },
    { Variant: '500', ColorHex: '#03A9F4', WhiteColor: false },
    { Variant: '600', ColorHex: '#039BE5', WhiteColor: false },
    { Variant: '700', ColorHex: '#0288D1', WhiteColor: true },
    { Variant: '800', ColorHex: '#0277BD', WhiteColor: true },
    { Variant: '900', ColorHex: '#01579B', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#80D8FF', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#40C4FF', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#00B0FF', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#0091EA', WhiteColor: true },
  ];

  /** Material: Cyan */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Cyan: Color[] = [
    { Variant: '50', ColorHex: '#E0F7FA', WhiteColor: false },
    { Variant: '100', ColorHex: '#B2EBF2', WhiteColor: false },
    { Variant: '200', ColorHex: '#80DEEA', WhiteColor: false },
    { Variant: '300', ColorHex: '#4DD0E1', WhiteColor: false },
    { Variant: '400', ColorHex: '#26C6DA', WhiteColor: false },
    { Variant: '500', ColorHex: '#00BCD4', WhiteColor: false },
    { Variant: '600', ColorHex: '#00ACC1', WhiteColor: false },
    { Variant: '700', ColorHex: '#0097A7', WhiteColor: true },
    { Variant: '800', ColorHex: '#00838F', WhiteColor: true },
    { Variant: '900', ColorHex: '#006064', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#84FFFF', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#18FFFF', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#00E5FF', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#00B8D4', WhiteColor: false },
  ];

  /** Material: Teal */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Teal: Color[] = [
    { Variant: '50', ColorHex: '#E0F2F1', WhiteColor: false },
    { Variant: '100', ColorHex: '#B2DFDB', WhiteColor: false },
    { Variant: '200', ColorHex: '#80CBC4', WhiteColor: false },
    { Variant: '300', ColorHex: '#4DB6AC', WhiteColor: false },
    { Variant: '400', ColorHex: '#26A69A', WhiteColor: false },
    { Variant: '500', ColorHex: '#009688', WhiteColor: true },
    { Variant: '600', ColorHex: '#00897B', WhiteColor: true },
    { Variant: '700', ColorHex: '#00796B', WhiteColor: true },
    { Variant: '800', ColorHex: '#00695C', WhiteColor: true },
    { Variant: '900', ColorHex: '#004D40', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#A7FFEB', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#64FFDA', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#1DE9B6', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#00BFA5', WhiteColor: false },
  ];

  /** Material: Green */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Green: Color[] = [
    { Variant: '50', ColorHex: '#E8F5E9', WhiteColor: false },
    { Variant: '100', ColorHex: '#C8E6C9', WhiteColor: false },
    { Variant: '200', ColorHex: '#A5D6A7', WhiteColor: false },
    { Variant: '300', ColorHex: '#81C784', WhiteColor: false },
    { Variant: '400', ColorHex: '#66BB6A', WhiteColor: false },
    { Variant: '500', ColorHex: '#4CAF50', WhiteColor: false },
    { Variant: '600', ColorHex: '#43A047', WhiteColor: true },
    { Variant: '700', ColorHex: '#388E3C', WhiteColor: true },
    { Variant: '800', ColorHex: '#2E7D32', WhiteColor: true },
    { Variant: '900', ColorHex: '#1B5E20', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#B9F6CA', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#69F0AE', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#00E676', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#00C853', WhiteColor: false },
  ];

  /** Material: LightGreen */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public LightGreen: Color[] = [
    { Variant: '50', ColorHex: '#F1F8E9', WhiteColor: false },
    { Variant: '100', ColorHex: '#DCEDC8', WhiteColor: false },
    { Variant: '200', ColorHex: '#C5E1A5', WhiteColor: false },
    { Variant: '300', ColorHex: '#AED581', WhiteColor: false },
    { Variant: '400', ColorHex: '#9CCC65', WhiteColor: false },
    { Variant: '500', ColorHex: '#8BC34A', WhiteColor: false },
    { Variant: '600', ColorHex: '#7CB342', WhiteColor: false },
    { Variant: '700', ColorHex: '#689F38', WhiteColor: false },
    { Variant: '800', ColorHex: '#558B2F', WhiteColor: true },
    { Variant: '900', ColorHex: '#33691E', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#CCFF90', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#B2FF59', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#76FF03', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#64DD17', WhiteColor: false },
  ];

  /** Material: Lime */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Lime: Color[] = [
    { Variant: '50', ColorHex: '#F9FBE7', WhiteColor: false },
    { Variant: '100', ColorHex: '#F0F4C3', WhiteColor: false },
    { Variant: '200', ColorHex: '#E6EE9C', WhiteColor: false },
    { Variant: '300', ColorHex: '#DCE775', WhiteColor: false },
    { Variant: '400', ColorHex: '#D4E157', WhiteColor: false },
    { Variant: '500', ColorHex: '#CDDC39', WhiteColor: false },
    { Variant: '600', ColorHex: '#C0CA33', WhiteColor: false },
    { Variant: '700', ColorHex: '#AFB42B', WhiteColor: false },
    { Variant: '800', ColorHex: '#9E9D24', WhiteColor: false },
    { Variant: '900', ColorHex: '#827717', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#F4FF81', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#EEFF41', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#C6FF00', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#AEEA00', WhiteColor: false },
  ];

  /** Material: Yellow */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Yellow: Color[] = [
    { Variant: '50', ColorHex: '#FFFDE7', WhiteColor: false },
    { Variant: '100', ColorHex: '#FFF9C4', WhiteColor: false },
    { Variant: '200', ColorHex: '#FFF59D', WhiteColor: false },
    { Variant: '300', ColorHex: '#FFF176', WhiteColor: false },
    { Variant: '400', ColorHex: '#FFEE58', WhiteColor: false },
    { Variant: '500', ColorHex: '#FFEB3B', WhiteColor: false },
    { Variant: '600', ColorHex: '#FDD835', WhiteColor: false },
    { Variant: '700', ColorHex: '#FBC02D', WhiteColor: false },
    { Variant: '800', ColorHex: '#F9A825', WhiteColor: false },
    { Variant: '900', ColorHex: '#F57F17', WhiteColor: false },
    { Variant: 'A100', ColorHex: '#FFFF8D', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#FFFF00', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#FFEA00', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#FFD600', WhiteColor: false },
  ];

  /** Material: Amber */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Amber: Color[] = [
    { Variant: '50', ColorHex: '#FFF8E1', WhiteColor: false },
    { Variant: '100', ColorHex: '#FFECB3', WhiteColor: false },
    { Variant: '200', ColorHex: '#FFE082', WhiteColor: false },
    { Variant: '300', ColorHex: '#FFD54F', WhiteColor: false },
    { Variant: '400', ColorHex: '#FFCA28', WhiteColor: false },
    { Variant: '500', ColorHex: '#FFC107', WhiteColor: false },
    { Variant: '600', ColorHex: '#FFB300', WhiteColor: false },
    { Variant: '700', ColorHex: '#FFA000', WhiteColor: false },
    { Variant: '800', ColorHex: '#FF8F00', WhiteColor: false },
    { Variant: '900', ColorHex: '#FF6F00', WhiteColor: false },
    { Variant: 'A100', ColorHex: '#FFE57F', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#FFD740', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#FFC400', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#FFAB00', WhiteColor: false },
  ];

  /** Material: Orange */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Orange: Color[] = [
    { Variant: '50', ColorHex: '#FFF3E0', WhiteColor: false },
    { Variant: '100', ColorHex: '#FFE0B2', WhiteColor: false },
    { Variant: '200', ColorHex: '#FFCC80', WhiteColor: false },
    { Variant: '300', ColorHex: '#FFB74D', WhiteColor: false },
    { Variant: '400', ColorHex: '#FFA726', WhiteColor: false },
    { Variant: '500', ColorHex: '#FF9800', WhiteColor: false },
    { Variant: '600', ColorHex: '#FB8C00', WhiteColor: false },
    { Variant: '700', ColorHex: '#F57C00', WhiteColor: false },
    { Variant: '800', ColorHex: '#EF6C00', WhiteColor: false },
    { Variant: '900', ColorHex: '#E65100', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#FFD180', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#FFAB40', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#FF9100', WhiteColor: false },
    { Variant: 'A700', ColorHex: '#FF6D00', WhiteColor: false },
  ];

  /** Material: DeepOrange */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public DeepOrange: Color[] = [
    { Variant: '50', ColorHex: '#FBE9E7', WhiteColor: false },
    { Variant: '100', ColorHex: '#FFCCBC', WhiteColor: false },
    { Variant: '200', ColorHex: '#FFAB91', WhiteColor: false },
    { Variant: '300', ColorHex: '#FF8A65', WhiteColor: false },
    { Variant: '400', ColorHex: '#FF7043', WhiteColor: false },
    { Variant: '500', ColorHex: '#FF5722', WhiteColor: false },
    { Variant: '600', ColorHex: '#F4511E', WhiteColor: true },
    { Variant: '700', ColorHex: '#E64A19', WhiteColor: true },
    { Variant: '800', ColorHex: '#D84315', WhiteColor: true },
    { Variant: '900', ColorHex: '#BF360C', WhiteColor: true },
    { Variant: 'A100', ColorHex: '#FF9E80', WhiteColor: false },
    { Variant: 'A200', ColorHex: '#FF6E40', WhiteColor: false },
    { Variant: 'A400', ColorHex: '#FF3D00', WhiteColor: true },
    { Variant: 'A700', ColorHex: '#DD2C00', WhiteColor: true },
  ];

  /** Material: Brown */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Brown: Color[] = [
    { Variant: '50', ColorHex: '#EFEBE9', WhiteColor: false },
    { Variant: '100', ColorHex: '#D7CCC8', WhiteColor: false },
    { Variant: '200', ColorHex: '#BCAAA4', WhiteColor: false },
    { Variant: '300', ColorHex: '#A1887F', WhiteColor: true },
    { Variant: '400', ColorHex: '#8D6E63', WhiteColor: true },
    { Variant: '500', ColorHex: '#795548', WhiteColor: true },
    { Variant: '600', ColorHex: '#6D4C41', WhiteColor: true },
    { Variant: '700', ColorHex: '#5D4037', WhiteColor: true },
    { Variant: '800', ColorHex: '#4E342E', WhiteColor: true },
    { Variant: '900', ColorHex: '#3E2723', WhiteColor: true },
  ];

  /** Material: Grey */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Grey: Color[] = [
    { Variant: '50', ColorHex: '#FAFAFA', WhiteColor: false },
    { Variant: '100', ColorHex: '#F5F5F5', WhiteColor: false },
    { Variant: '200', ColorHex: '#EEEEEE', WhiteColor: false },
    { Variant: '300', ColorHex: '#E0E0E0', WhiteColor: false },
    { Variant: '400', ColorHex: '#BDBDBD', WhiteColor: false },
    { Variant: '500', ColorHex: '#9E9E9E', WhiteColor: false },
    { Variant: '600', ColorHex: '#757575', WhiteColor: true },
    { Variant: '700', ColorHex: '#616161', WhiteColor: true },
    { Variant: '800', ColorHex: '#424242', WhiteColor: true },
    { Variant: '900', ColorHex: '#212121', WhiteColor: true },
  ];

  /** Material: BlueGrey */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public BlueGrey: Color[] = [
    { Variant: '50', ColorHex: '#ECEFF1', WhiteColor: false },
    { Variant: '100', ColorHex: '#CFD8DC', WhiteColor: false },
    { Variant: '200', ColorHex: '#B0BEC5', WhiteColor: false },
    { Variant: '300', ColorHex: '#90A4AE', WhiteColor: false },
    { Variant: '400', ColorHex: '#78909C', WhiteColor: true },
    { Variant: '500', ColorHex: '#607D8B', WhiteColor: true },
    { Variant: '600', ColorHex: '#546E7A', WhiteColor: true },
    { Variant: '700', ColorHex: '#455A64', WhiteColor: true },
    { Variant: '800', ColorHex: '#37474F', WhiteColor: true },
    { Variant: '900', ColorHex: '#263238', WhiteColor: true },
  ];

  /** Zusammenfassung aller Material Palettes */
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public Palettes = [
    /** Material */
    this.Red,
    this.Pink,
    this.Purple,
    this.DeepPurple,
    this.Indigo,
    this.Blue,
    this.LightBlue,
    this.Cyan,
    this.Teal,
    this.Green,
    this.LightGreen,
    this.Lime,
    this.Yellow,
    this.Amber,
    this.Orange,
    this.DeepOrange,
    this.Brown,
    this.Grey,
    this.BlueGrey,
  ];
}
