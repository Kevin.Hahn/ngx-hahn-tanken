import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SearchInputService {
  fuel = 'all';
  radius = 25;
  lowestPrice = false;
  stationFilter = 'all';
}
