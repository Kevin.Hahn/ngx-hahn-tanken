import { HttpClient } from '@angular/common/http';
import { ElementRef, Injectable } from '@angular/core';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Response200, Station } from '@api/tankerkoenig/model/models';
import { Errortype } from '@model/errortype.enum';
import { Feedback } from '@model/feedback.model';
import { Improvement } from '@model/improvement.model';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class StationService {
  currentAdress: google.maps.GeocoderResult[];
  currentData: Station[] | null;
  currentDate = new Date();
  currentFavorites = new Array<Station>();
  currentHiddenStations = new Array<Station>();
  changesToData = new Subject<any>();

  maxE5_Id: string;
  minE5_Id: string;
  maxE10_Id: string;
  minE10_Id: string;
  maxDiesel_Id: string;
  minDiesel_Id: string;

  filteredStations: string[] = [];
  isLoading = false;
  hasError = false;
  noSpecificAdress = false;
  isCardMode = true;

  constructor(private http: HttpClient) {
    this.isCardMode = localStorage.getItem('card-mode') === '' || localStorage.getItem('card-mode') === 'true';
    const hiddenStations = localStorage.getItem('hidden-stations');
    if (hiddenStations && hiddenStations !== '') {
      this.filteredStations = hiddenStations.split(',');
    }
  }

  sendComplaint(station: Station, verbesserung: Improvement, input: MatCheckbox | ElementRef, lat?: ElementRef, lng?: ElementRef) {
    const url = `https://creativecommons.tankerkoenig.de/json/complaint.php?`;
    let newValue: any;

    if (verbesserung.type === Errortype.wrongPetrolStationLocation) {
      newValue = lat.nativeElement.value + ',' + lng.nativeElement.value;
    } else {
      if (input instanceof MatCheckbox) {
        newValue = input.checked;
      } else {
        newValue = input.nativeElement.value;
      }
    }

    const data = {
      apikey: environment.tanken_api.API_KEY,
      id: station.id,
      type: verbesserung.type,
      correction: newValue,
    };

    return this.http.post(url, data);
  }

  sendFeedback(feedback: Feedback) {
    const url = environment.tanken_api.api + `mail/sendFeedback`;
    return this.http.post(url, feedback);
  }

  prepareDaten(daten: Response200, tankstellenfilter: string) {
    if (daten != null) {
      this.currentHiddenStations = daten.stations.filter((station) => this.filteredStations.indexOf(station.id) !== -1);
      daten.stations = daten.stations.filter((station) => this.filteredStations.indexOf(station.id) === -1);

      if (tankstellenfilter !== 'all') {
        daten.stations = this.filterStationsByBrandOrGroup(daten.stations, tankstellenfilter);
      }

      this.preparePrices(daten);
      this.prepareFavorites(daten);
      daten.stations.sort(this.sortByDistance);
      this.currentData = daten.stations;
    }
  }

  prepareData(data: Station[], sort: MatSort, paginator: MatPaginator) {
    const stationsData = new MatTableDataSource<Station>(data);
    stationsData.paginator = paginator;

    stationsData.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'e5': {
          const e5Data = item.fuels.filter((x) => x.name === 'Super E5');
          return e5Data.length > 0 ? e5Data[0].price : null;
        }
        case 'e10': {
          const e10Data = item.fuels.filter((x) => x.name === 'Super E10');
          return e10Data.length > 0 ? e10Data[0].price : null;
        }
        case 'diesel': {
          const dieselData = item.fuels.filter((x) => x.name === 'Diesel');
          return dieselData.length > 0 ? dieselData[0].price : null;
        }
        default:
          return item[property];
      }
    };

    stationsData.sort = sort;
    return stationsData;
  }

  sortByDistance(a, b) {
    if (a.dist < b.dist) {
      return -1;
    }
    if (a.dist > b.dist) {
      return 1;
    }
    return 0;
  }

  sortByPrice(a, b) {
    if (a.price < b.price) {
      return -1;
    }
    if (a.price > b.price) {
      return 1;
    }
    return 0;
  }

  preparePrices(daten: Response200) {
    const e5Data = new Array<any>();
    const e10Data = new Array<any>();
    const dieselData = new Array<any>();

    daten.stations.forEach((element) => {
      const e5 = element.fuels.filter((f) => f.name === 'Super E5');
      if (e5.length > 0) {
        e5Data.push({ id: element.id, price: e5[0].price });
      }

      const e10 = element.fuels.filter((f) => f.name === 'Super E10');
      if (e10.length > 0) {
        e10Data.push({ id: element.id, price: e10[0].price });
      }

      const diesel = element.fuels.filter((f) => f.name === 'Diesel');
      if (diesel.length > 0) {
        dieselData.push({ id: element.id, price: diesel[0].price });
      }
    });

    e5Data.sort(this.sortByPrice);
    e10Data.sort(this.sortByPrice);
    dieselData.sort(this.sortByPrice);

    this.minE5_Id = e5Data[0].id;
    this.maxE5_Id = e5Data[e5Data.length - 1].id;
    this.minE10_Id = e10Data[0].id;
    this.maxE10_Id = e10Data[e10Data.length - 1].id;
    this.minDiesel_Id = dieselData[0].id;
    this.maxDiesel_Id = dieselData[dieselData.length - 1].id;
  }

  filterStationsByBrandOrGroup(stations: Station[], tankstellenfilter: string) {
    switch (tankstellenfilter) {
      // filter for single stations by brand
      case 'only-aral':
        stations = stations.filter((x) => x.brand.toLocaleLowerCase().indexOf('aral') > -1);
        break;
      case 'only-shell':
        stations = stations.filter((x) => x.brand.toLocaleLowerCase().indexOf('shell') > -1);
        break;
      case 'only-raiffeisen':
        stations = stations.filter((x) => x.brand.toLocaleLowerCase().indexOf('raiffeisen') > -1);
        break;
      // filter for multiple stations by brand
      case 'all-big':
        stations = stations.filter(
          (x) => x.brand.toLocaleLowerCase().indexOf('aral') > -1 || x.brand.toLocaleLowerCase().indexOf('shell') > -1 || x.brand.toLocaleLowerCase().indexOf('raiffeisen') > -1
        );
        break;
      case 'all-small':
        stations = stations.filter((x) => x.brand.toLocaleLowerCase().indexOf('aral') < 0 && x.brand.toLocaleLowerCase().indexOf('shell') < 0 && x.brand.toLocaleLowerCase().indexOf('raiffeisen') < 0);
        break;
    }

    return stations;
  }

  prepareFavorites(daten: Response200) {
    const cookieFavorites = localStorage.getItem('favorites');
    if (cookieFavorites && cookieFavorites !== '') {
      const favorites = cookieFavorites.split(',');
      this.currentFavorites = daten.stations.filter((x) => favorites.indexOf(x.id) !== -1);
    }
  }

  setCardMode(isCardMode: boolean) {
    this.isCardMode = isCardMode;
    localStorage.setItem('card-mode', isCardMode.toString());
  }

  getHiddenStationsFromCookies() {
    const hiddenStations = localStorage.getItem('hidden-stations');
    if (hiddenStations && hiddenStations !== '') {
      return hiddenStations.split(',');
    } else {
      return new Array<string>();
    }
  }

  hideStation(station: Station) {
    const hiddenStations = this.getHiddenStationsFromCookies();

    hiddenStations.push(station.id);
    this.filteredStations = hiddenStations;
    localStorage.setItem('hidden-stations', hiddenStations.join(','));

    this.currentHiddenStations.push(station);
    this.currentData = this.currentData.filter((x) => x.id !== station.id);
  }

  showStation(station: Station) {
    let hiddenStations = this.getHiddenStationsFromCookies();
    hiddenStations = hiddenStations.filter((c) => c !== station.id);

    localStorage.setItem('hidden-stations', hiddenStations.join(','));
    this.filteredStations = hiddenStations;
    this.currentHiddenStations = this.currentHiddenStations.filter((s) => s.id !== station.id);

    this.currentData.push(station);
  }

  getStationNearClass(dist: number): string {
    // TODO: Magic numbers needs to be configureable by user account to let the user determine which station is good or bad
    if (dist <= 7 && dist > 3) {
      return 'station-range-2';
    }

    if (dist <= 15 && dist > 7) {
      return 'station-range-3';
    }

    if (dist > 15) {
      return 'station-range-4';
    }

    return 'station-range-1';
  }

  getStationNearTooltip(dist: number): string {
    // TODO: Magic numbers needs to be configureable by user account to let the user determine which station is good or bad
    if (dist <= 15 && dist > 7) {
      return 'DIST_MID';
    }

    if (dist > 15) {
      return 'DIST_HIGH';
    }

    return 'DIST_LOW';
  }
}
