import { Injectable } from '@angular/core';

@Injectable()
export class DisclaimerService {
  disclaimerAccepted = false;

  constructor() {
    this.getDisclaimerAccepted();
  }

  getDisclaimerAccepted() {
    const disclaimerAccepted = localStorage.getItem('disclaimerAccepted');
    if (disclaimerAccepted && disclaimerAccepted !== '') {
      this.disclaimerAccepted = disclaimerAccepted as unknown as boolean;
    }
  }

  setDisclaimerAccepted(value: boolean) {
    this.disclaimerAccepted = value;
  }

  updateDisclaimerAccepted(value: boolean) {
    this.setDisclaimerAccepted(value);
    localStorage.setItem('disclaimerAccepted', value.toString());
  }
}
