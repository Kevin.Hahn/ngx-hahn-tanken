import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Station } from '@api/tankerkoenig/model/station';
import { TranslateService } from '@ngx-translate/core';

import { StationService } from './station.service';

@Injectable()
export class FavoritesService {
  constructor(private stationService: StationService, private translate: TranslateService, private snackBar: MatSnackBar) {}

  getFavoritText(element: Station) {
    return this.isInFavorites(element.id) ? 'REMOVE' : 'ADD';
  }

  getFavoriteStarColor(element: Station) {
    return this.isInFavorites(element.id) ? 'text-yellow' : 'text-gray';
  }

  isInFavorites(Id: string): boolean {
    const cookieFavorites = localStorage.getItem('favorites');
    if (cookieFavorites && cookieFavorites !== '') {
      const favorites = cookieFavorites.split(',');
      return favorites.filter((fav) => fav === Id).length > 0;
    } else {
      return false;
    }
  }

  setFavorite(station: Station) {
    const cookieFavorites = localStorage.getItem('favorites');
    let favorites = [];
    if (cookieFavorites && cookieFavorites !== '') {
      favorites = cookieFavorites.split(',');
    }

    if (!this.isInFavorites(station.id)) {
      favorites.push(station.id);
      this.stationService.currentFavorites.push(station);
      localStorage.setItem('favorites', favorites.join(','));
      this.translate.get('SNACKBAR.STATION_ADD_FAV').subscribe((res: string) => {
        this.snackBar.open(res, 'OK', { duration: 5000 });
      });
    } else {
      this.deleteFromFavorites(station);
    }
    this.stationService.changesToData.next(null);
  }

  deleteFromFavorites(station: Station) {
    const cookieFavorites = localStorage.getItem('favorites');
    let favorites = [];
    if (cookieFavorites && cookieFavorites !== '') {
      favorites = cookieFavorites.split(',');
    }
    localStorage.setItem('favorites', favorites.filter((x) => x !== station.id).join(','));
    this.stationService.currentFavorites = this.stationService.currentFavorites.filter((x) => x.id !== station.id);
    this.stationService.changesToData.next(null);

    this.translate.get('SNACKBAR.STATION_DEL_FAV').subscribe((res: string) => {
      this.snackBar.open(res, 'OK', { duration: 5000 });
    });
  }
}
