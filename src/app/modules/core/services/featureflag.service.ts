import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

type Featureflags = 'no_login_functionality';

@Injectable({
  providedIn: 'root',
})
export class FeatureflagService {
  constructor(private http: HttpClient) {}

  isFeatureEnabled(flag: Featureflags): Observable<boolean> {
    const url = environment.tanken_api.api + `tanken-featureflags/getFeatureflag/${flag}`;
    return this.http.get(url).pipe(map((x) => <boolean>x));
  }
}
