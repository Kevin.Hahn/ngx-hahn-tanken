import { Component, Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

/** Daten für den letzten Dialog */
class LastDialogData {
  /** Komponentenreferenz */
  component: any;
  /** Daten des Dialogs */
  data: any;
  /** Fokuselement */
  focus: any;
  /** Breite des Dialogs */
  width: string;
  /** Schließen verhindern? */
  disableClose: boolean;
  /** Custom Class des Dialogs */
  customClass: string;
}

/** Dialogservice */
@Injectable()
export class DialogService {
  dialogRef: MatDialogRef<Component>;
  submit: any;
  lastDialogData: LastDialogData = null;
  lastDialogChain: LastDialogData[] = [];

  constructor(private dialog: MatDialog) {}

  /**
   * Schließt den aktuell geöffneten Dialog und öffnet einen neuen mit den gelieferten Daten
   * @param component Beliebige AngularComponent
   * @param data Beliebige Daten
   * @param focus Focus auf ein bestimmtes Element?
   * @param width Breite des Modals
   * @param disableClose Schließen verhindern?
   * @param customCSSClass additionelle CSS Klasse?
   */
  openDialog(component: any, data?: any, focus?: any, width = '900px', disableClose?: boolean, customCSSClass = '') {
    this.dialog.closeAll();
    const fullData = {
      data: data,
      focus: focus,
    };

    const panelClass = new Array<string>();
    panelClass.push('dialog-wrapper');
    if (customCSSClass !== '') {
      panelClass.push(customCSSClass);
    }

    this.dialogRef = this.dialog.open(component, {
      maxWidth: '90vw',
      width: width,
      maxHeight: '95vh',
      data: fullData,
      position: { top: '10px' },
      autoFocus: false,
      panelClass: panelClass,
      disableClose: disableClose,
    });

    this.addLastDialogData(component, data, focus, width, disableClose, customCSSClass);
    this.dialogRef.afterClosed().subscribe(() => this.submit);
    this.dialog.afterAllClosed.subscribe(() => this.clearLastDialogChain());
  }

  addLastDialogData(component: any, data: any, focus: any, width: string, disableClose: boolean, customCSSClass: string) {
    const kontext = new LastDialogData();
    kontext.component = component;
    kontext.data = data;
    kontext.focus = focus;
    kontext.width = width;
    kontext.disableClose = disableClose;
    kontext.customClass = customCSSClass;

    this.lastDialogChain.push(kontext);
  }

  goToLastDialog() {
    const lastDialog = this.lastDialogChain[this.lastDialogChain.length - 2];
    this.lastDialogChain.splice(this.lastDialogChain.length - 2, 1);

    if (lastDialog) {
      this.openDialog(lastDialog.component, lastDialog.data, lastDialog.focus, lastDialog.width, lastDialog.disableClose, lastDialog.customClass);
    }
  }

  clearLastDialogChain() {
    this.lastDialogData = null;
    this.lastDialogChain = [];
  }

  closeDialog(result = null) {
    this.dialogRef.close(result);
  }

  onClose() {
    return this.dialogRef.afterClosed();
  }
}
