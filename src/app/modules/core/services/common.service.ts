import { Injectable } from '@angular/core';
import { Fuel } from '@api/tankerkoenig/model/fuel';
import { Station } from '@api/tankerkoenig/model/station';
import { FuelType } from '@model/fueltype.enum';

@Injectable()
export class CommonService {
  /**
   * helper method to sup the last number of the fuel price
   * @param price price for fuel
   * @returns html formatted string to add missing trailing 0 and sub the last number

   */
  getLastCharSup(price: number) {
    if (price !== undefined && price !== null) {
      const value = String(price).replace('.', ',');
      let front = value.slice(0, value.length - 1);
      if (front.length == 3) {
        front += '0';
      }
      return front + '<sup class="price-subbed">' + value.slice(value.length - 1, value.length) + '</sup> €';
    } else {
      return '---';
    }
  }

  /**
   * Preperation of fuel data for given fueltype
   * @param fuelkey fueltype
   * @param station station to lookup
   * @returns target fuel from station
   */
  getFuelByType(fueltype: FuelType, station: Station) {
    const fuels = station.fuels.filter((f) => f.name === fueltype) as Fuel[];
    if (fuels.length === 1) {
      return fuels[0];
    } else {
      return null;
    }
  }
}
