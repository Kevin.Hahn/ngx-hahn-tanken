import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TankerkoenigSearchService } from '@api/tankerkoenig/api/TankerkoenigSearch.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

import { GeocodingService } from './geocoding.service';
import { SearchInputService } from './search-input.service';
import { StationService } from './station.service';

@Injectable()
export class SearchService {
  constructor(
    private geoCodingService: GeocodingService,
    private snackBar: MatSnackBar,
    private stationService: StationService,
    private searchInputService: SearchInputService,
    private router: Router,
    private translate: TranslateService,
    private searchService: TankerkoenigSearchService
  ) {}

  async startSearch(adresse) {
    this.stationService.isLoading = true;
    this.stationService.hasError = false;

    try {
      const adressData = await this.geoCodingService.codeAddress(adresse).toPromise();
      this.stationService.currentAdress = adressData;
      const geodata = adressData[0].geometry.location.toJSON();

      const stationQuery = await this.searchService.stationsSearchGet(environment.tanken_api.API_KEY, geodata.lat, geodata.lng, this.searchInputService.radius).toPromise();

      this.stationService.prepareDaten(stationQuery, this.searchInputService.stationFilter);
      this.stationService.changesToData.next(null);
      this.stationService.isLoading = false;

      if (this.stationService.currentData.length === 0) {
        this.translate.get('SNACKBAR.NO_DATA_POSITION').subscribe((res: string) => {
          this.snackBar.open(res, 'OK', { duration: 5000 });
        });
      }
    } catch (e) {
      this.stationService.isLoading = false;
      this.stationService.hasError = true;
      this.stationService.currentData = null;
      console.log('Fehler bei der Suche: ', e);
    }

    if (this.stationService.isCardMode) {
      this.router.navigate(['stations/cards']);
    } else {
      this.router.navigate(['stations/list']);
    }
  }
}
