import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import IPriceChange from '@model/pricechange.interface';
import IStatus from '@model/status.interface';
import { TimeSpan } from '@model/timespan.enum';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class StationHistoryService {
  apiOnline = false;
  lastStatusDate: Date;
  lastWeekDate: Date;
  lastTwoWeekDate: Date;

  constructor(private http: HttpClient) {}

  isOnline(): Observable<any> {
    return this.getLastUpdated();
  }

  getLastUpdated(): Observable<IStatus> {
    const url = environment.tanken_api.api + `status/lastUpdated`;
    return this.http.get(url).pipe(map((x) => <IStatus>x));
  }

  getDataForTimeSpan(timeSpan: TimeSpan, stationId: string) {
    let url = environment.tanken_api.api + `station/`;
    switch (timeSpan) {
      case TimeSpan.DAY:
        url += `lastDay/${stationId}`;
        break;
      case TimeSpan.WEEK:
        url += `lastWeek/${stationId}`;
        break;
      case TimeSpan.TWO_WEEKS:
        url += `lastTwoWeeks/${stationId}`;
        break;
      case TimeSpan.MONTH:
        throw new Error('Method for TimeSpan.MONTH not implemented.');
    }
    return this.http.get(url).pipe(map((x) => <Array<IPriceChange[]>>x));
  }

  updateApiData(): Observable<any> {
    const url = environment.tanken_api.api + `status/updateStatus`;
    return this.http.post(url, {});
  }
}
