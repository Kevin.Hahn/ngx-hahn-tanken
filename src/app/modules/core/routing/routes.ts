import { Routes } from '@angular/router';
import { DisclaimerComponent } from 'src/app/components/disclaimer/disclaimer.component';
import { FavoritesMapComponent } from 'src/app/components/favorite-map/favorite-map.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { StationCardsComponent } from 'src/app/components/station-cards/station-cards.component';
import { StationListComponent } from 'src/app/components/station-list/station-list.component';
import { UserFeedbackComponent } from 'src/app/components/user-feedback/user-feedback.component';

import { DataGuard } from './data.guard';

export const routes: Routes = [
  { path: 'disclaimer', component: DisclaimerComponent },
  { path: 'feedback', component: UserFeedbackComponent },
  { path: 'favoritemap', component: FavoritesMapComponent },
  {
    path: 'stations/list',
    component: StationListComponent,
    canActivate: [DataGuard],
  },
  {
    path: 'stations/cards',
    component: StationCardsComponent,
    canActivate: [DataGuard],
  },
  { path: '', component: HomeComponent },
];
