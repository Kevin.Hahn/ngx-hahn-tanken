import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { StationService } from '../services/station.service';

@Injectable()
export class DataGuard implements CanActivate {
  constructor(private stationService: StationService, public router: Router) {}

  canActivate() {
    if (this.stationService.currentData) {
      return true;
    } else {
      return this.router.parseUrl('');
    }
  }
}
