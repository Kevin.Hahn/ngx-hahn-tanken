import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeResourceUrl, SafeScript, SafeStyle, SafeUrl } from '@angular/platform-browser';

@Pipe({ name: 'ReplaceEmpty' })
export class ReplaceEmptyPipe implements PipeTransform {
  transform(value: string, text = ''): string {
    if (!value || value === '') {
      if (text === '') {
        return '---';
      } else {
        return text;
      }
    } else {
      return value;
    }
  }
}

@Pipe({
  name: 'enumToArray',
})
export class EnumToArrayPipe implements PipeTransform {
  transform(data: Record<string, unknown>) {
    return Object.keys(data);
  }
}

@Pipe({
  name: 'apiDateFormat',
})
export class ApiDateFormatPipe extends DatePipe implements PipeTransform {
  transform(value: any): any {
    return super.transform(value, 'yyyy-MM-dd hh:mm:ss');
  }
}

@Pipe({
  name: 'safe',
})
export class SafePipe implements PipeTransform {
  constructor(protected sanitizer: DomSanitizer) {}

  public transform(value: any, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
    switch (type) {
      case 'html':
        return this.sanitizer.bypassSecurityTrustHtml(value);
      case 'style':
        return this.sanitizer.bypassSecurityTrustStyle(value);
      case 'script':
        return this.sanitizer.bypassSecurityTrustScript(value);
      case 'url':
        return this.sanitizer.bypassSecurityTrustUrl(value);
      case 'resourceUrl':
        return this.sanitizer.bypassSecurityTrustResourceUrl(value);
      default:
        throw new Error(`Invalid safe type specified: ${type}`);
    }
  }
}
