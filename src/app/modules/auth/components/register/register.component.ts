import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from '@coreService/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm: UntypedFormGroup;

  constructor(private builder: UntypedFormBuilder, private dialogService: DialogService, private authService: AuthService, private snackBar: MatSnackBar, private translate: TranslateService) {}

  ngOnInit() {
    this.registerForm = this.builder.group({
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password2: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  register(email: string, password: string) {
    this.authService.register({ email, password }).then(() => {
      this.translate.get('SNACKBAR.AUTH.VERIFYSEND').subscribe((res: string) => {
        this.snackBar.open(res, 'OK', { duration: 10000 });
      });
      this.dialogService.closeDialog();
    });
  }

  loadLogin() {
    this.dialogService.goToLastDialog();
  }
}
