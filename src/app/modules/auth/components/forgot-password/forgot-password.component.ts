import { Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { DialogService } from '@core/services/dialog.service';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent {
  resetForm: UntypedFormGroup;

  constructor(private authService: AuthService, private dialogService: DialogService, private builder: UntypedFormBuilder) {
    this.resetForm = this.builder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  forgotPassword(email: string) {
    this.authService.forgotPassword(email);
    this.dialogService.closeDialog();
  }

  loadLogin() {
    this.dialogService.goToLastDialog();
  }
}
