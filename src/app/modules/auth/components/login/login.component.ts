import { Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import { DialogService } from '@coreService/dialog.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { environment } from 'src/environments/environment';

import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  loginForm: UntypedFormGroup;

  constructor(private builder: UntypedFormBuilder, public authService: AuthService, private dialogService: DialogService, private translate: TranslateService, private snackBar: MatSnackBar) {
    this.loginForm = this.builder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  login(email: string, password: string) {
    this.authService
      .login({ email, password })
      .then(() => {
        this.dialogService.closeDialog();
        this.welcomeBackMessage();
      })
      .catch((error) => {
        this.translate.get('SNACKBAR.AUTH.LOGINFAIL').subscribe((res: string) => {
          this.snackBar.open(res, 'OK', { duration: 10000 });
        });
        if (!environment.production) {
          console.error(error);
        }
      });
  }

  loginWithGoogle() {
    this.authService.loginWithGoogle().then(() => {
      this.dialogService.closeDialog();
      this.welcomeBackMessage();
    });
  }

  loginWithFacebook() {
    this.authService.loginWithFacebook().then(() => {
      this.dialogService.closeDialog();
      this.welcomeBackMessage();
    });
  }

  loadRegister() {
    this.dialogService.openDialog(RegisterComponent);
  }

  loadForgotPassword() {
    this.dialogService.openDialog(ForgotPasswordComponent);
  }

  welcomeBackMessage() {
    this.translate.get('SNACKBAR.AUTH.WELCOMEBACK').subscribe((res: string) => {
      this.snackBar.open(res, 'OK', { duration: 5000 });
    });
  }
}
