/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DialogService } from '@core/services/dialog.service';
import { TranslateModule } from '@ngx-translate/core';
import { TestDependenciesModule } from 'src/test/modules/test-dependencies/test-dependencies.module';

import { AuthService } from '../../services/auth.service';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  const authServiceStub = {};
  const dialogServiceStub = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestDependenciesModule, TranslateModule.forRoot()],
      declarations: [LoginComponent],
      providers: [
        { provide: AuthService, useValue: authServiceStub },
        { provide: DialogService, useValue: dialogServiceStub },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
