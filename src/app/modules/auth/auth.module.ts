import { NgModule } from '@angular/core';

import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideDatabase, getDatabase } from '@angular/fire/database';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { TranslateModule } from '@ngx-translate/core';
import { AuthService } from './services/auth.service';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DependenciesModule } from '@core/dependencies.module';

const components = [ForgotPasswordComponent, LoginComponent, RegisterComponent];

@NgModule({
  imports: [
    DependenciesModule,
    TranslateModule.forChild(),
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAuth(() => getAuth()),
    provideDatabase(() => getDatabase()),
    provideFirestore(() => getFirestore()),
  ],
  declarations: [...components],
  exports: [...components],
  providers: [AuthService],
})
export class AuthModule {
  static components = {
    login: LoginComponent,
    register: RegisterComponent,
    forgotpassword: ForgotPasswordComponent,
  };
}
