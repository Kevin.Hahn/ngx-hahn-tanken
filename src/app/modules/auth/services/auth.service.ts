import {
  Auth,
  createUserWithEmailAndPassword,
  FacebookAuthProvider,
  GoogleAuthProvider,
  signInWithEmailAndPassword,
  sendPasswordResetEmail,
  signInWithPopup,
  signOut,
  User,
  sendEmailVerification,
  onAuthStateChanged,
} from '@angular/fire/auth';

import { Injectable } from '@angular/core';
import { LoginData } from '../interfaces/loginData.interface';

import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  currentUser = new BehaviorSubject<User>(null);
  user: User;

  constructor(private auth: Auth, private snackBar: MatSnackBar, private router: Router, private translate: TranslateService) {
    this.currentUser.subscribe((user) => {
      this.user = user;
    });

    this.checkAuthState();
  }

  checkAuthState() {
    onAuthStateChanged(this.auth, (user) => {
      this.currentUser.next(user);
    });
  }

  login({ email, password }: LoginData) {
    return signInWithEmailAndPassword(this.auth, email, password);
  }

  register({ email, password }: LoginData) {
    return createUserWithEmailAndPassword(this.auth, email, password)
      .then((userCredential) => {
        sendEmailVerification(userCredential.user).then(() => {
          this.router.navigate(['verify-email-address']);
        });
      })
      .catch((error) => {
        this.translate.get('SNACKBAR.AUTH.REGISTERFAIL').subscribe((res: string) => {
          this.snackBar.open(res, 'OK', { duration: 10000 });
        });
        if (!environment.production) {
          console.error(error);
        }
      });
  }

  loginWithGoogle() {
    return signInWithPopup(this.auth, new GoogleAuthProvider());
  }

  loginWithFacebook() {
    return signInWithPopup(this.auth, new FacebookAuthProvider());
  }

  logout() {
    this.currentUser.next(null);
    return signOut(this.auth);
  }

  forgotPassword(email: string) {
    sendPasswordResetEmail(this.auth, email)
      .then(() => {
        this.translate.get('SNACKBAR.AUTH.VERIFYSEND').subscribe((res: string) => {
          this.snackBar.open(res, 'OK', { duration: 10000 });
        });
      })
      .catch((error) => {
        this.translate.get('SNACKBAR.AUTH.VERIFYSENDFAIL').subscribe((res: string) => {
          this.snackBar.open(res, 'OK', { duration: 10000 });
        });
        if (!environment.production) {
          console.error(error);
        }
      });
  }
}
