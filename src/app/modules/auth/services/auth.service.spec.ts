/* eslint-disable @typescript-eslint/no-unused-vars */
import { inject, TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

describe('Service: Auth', () => {
  const authServiceStub = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: AuthService, useValue: authServiceStub }],
    });
  });

  it('should ...', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));
});
