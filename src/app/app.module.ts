import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { GoogleMapsModule } from '@angular/google-maps';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TankerkoenigApiModule } from '@api/tankerkoenig/tankerkoenig-api.module';
import { DependenciesModule } from '@core/dependencies.module';
import { FeatureflagService } from '@core/services/featureflag.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Angulartics2Module } from 'angulartics2';
import { NgChartsModule } from 'ng2-charts';

import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { DisclaimerBoxComponent } from './components/disclaimer-box/disclaimer-box.component';
import { DisclaimerComponent } from './components/disclaimer/disclaimer.component';
import { FavoritesMapComponent } from './components/favorite-map/favorite-map.component';
import { FavoritenPinComponent } from './components/favoriten-pin/favoriten-pin.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { HomeComponent } from './components/home/home.component';
import { SearchOptionsComponent } from './components/search-options/search-options.component';
import { SearchComponent } from './components/search/search.component';
import { CardDetailComponent } from './components/station-cards/card-detail/card-detail.component';
import { CardHistoryComponent } from './components/station-cards/card-history/card-history.component';
import { CardComponent } from './components/station-cards/card/card.component';
import { StationCardsComponent } from './components/station-cards/station-cards.component';
import { StationListComponent } from './components/station-list/station-list.component';
import { StationTableComponent } from './components/station-list/station-table/station-table.component';
import { StatsComponent } from './components/stats/stats.component';
import { UserFeedbackComponent } from './components/user-feedback/user-feedback.component';
import { AuthModule } from './modules/auth/auth.module';
import { CoreModule } from './modules/core/core.module';
import { AppRoutingModule } from './modules/core/routing/app-routing.module';
import { DataGuard } from './modules/core/routing/data.guard';
import { UserModule } from './modules/domain/user/user.module';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    SearchComponent,
    FeedbackComponent,
    CardDetailComponent,
    SearchOptionsComponent,
    CardHistoryComponent,
    HomeComponent,
    StationListComponent,
    StationTableComponent,
    StationCardsComponent,
    DisclaimerComponent,
    DisclaimerBoxComponent,
    UserFeedbackComponent,
    StatsComponent,
    FavoritesMapComponent,
    FavoritenPinComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ScrollingModule,
    TankerkoenigApiModule,
    GoogleMapsModule,
    NgChartsModule,
    Angulartics2Module.forRoot({
      pageTracking: {
        clearQueryParams: true,
      },
      ga: {
        anonymizeIp: true,
      },
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    DependenciesModule,
    UserModule,
    AuthModule,
  ],
  providers: [DataGuard, FeatureflagService],
  bootstrap: [AppComponent],
})
export class AppModule {}
