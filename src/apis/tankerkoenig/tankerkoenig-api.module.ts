import { HttpClient } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

import { TankerkoenigComplaintService } from './api/TankerkoenigComplaint.service';
import { TankerkoenigSearchService } from './api/TankerkoenigSearch.service';
import { TankerkoenigStatisticsService } from './api/TankerkoenigStatistics.service';
import { Configuration } from './configuration';

@NgModule({
  imports: [],
  declarations: [],
  exports: [],
  providers: [TankerkoenigComplaintService, TankerkoenigSearchService, TankerkoenigStatisticsService],
})
export class TankerkoenigApiModule {
  public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders<TankerkoenigApiModule> {
    return {
      ngModule: TankerkoenigApiModule,
      providers: [{ provide: Configuration, useFactory: configurationFactory }],
    };
  }

  constructor(@Optional() @SkipSelf() parentModule: TankerkoenigApiModule, @Optional() http: HttpClient) {
    if (parentModule) {
      throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
    }
    if (!http) {
      throw new Error('You need to import the HttpClientModule in your AppModule! \n' + 'See also https://github.com/angular/angular/issues/20575');
    }
  }
}
