import { TankerkoenigComplaintService } from './TankerkoenigComplaint.service';
import { TankerkoenigSearchService } from './TankerkoenigSearch.service';
import { TankerkoenigStatisticsService } from './TankerkoenigStatistics.service';

export * from "./TankerkoenigComplaint.service";
export * from "./TankerkoenigSearch.service";
export * from "./TankerkoenigStatistics.service";
export const APIS = [
  TankerkoenigComplaintService,
  TankerkoenigSearchService,
  TankerkoenigStatisticsService,
];
