// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  adress: 'Bismarckstraße 45, 39517 Tangerhütte',
  version: require('../../package.json').version,
  tanken_api: {
    api: 'https://der-hahn-api.de/',
    API_KEY: '1712c750-3bf7-92bd-cb86-b42f3386df54',
    API_KEY_STATS: 'cffa4fb8-7a16-cd85-7946-263722530f15',
  },
  firebase: {
    apiKey: 'AIzaSyBCn3M79_6lIYvOkgn9ZeXNGJIv-5pUVaE',
    authDomain: 'ngxtanken-dev.firebaseapp.com',
    databaseURL: 'https://ngxtanken-dev.firebaseio.com',
    projectId: 'ngxtanken-dev',
    storageBucket: 'ngxtanken-dev.appspot.com',
    messagingSenderId: '443040304661',
    appId: '1:443040304661:web:fee43a9ec84c17e0e4081c',
    measurementId: 'G-V3FHKFBCYM',
  },
};
