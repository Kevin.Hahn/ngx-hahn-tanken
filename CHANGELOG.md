# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.9.7](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/compare/v0.9.6...v0.9.7) (2022-12-11)


### Features

* [#51](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/issues/51) moved disclaimer to footer ([3e8ef41](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/3e8ef41c3e5a88732d0056bf5249de968abcfe04))
* netcup rewrite rules ([e53245d](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/e53245d4fe4a95b026e6a98bb55567f067050d52))
* ng 14! ([189880f](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/189880f3de3d9d815a61b2ae48fe435ab5317f56))
* ng material 14 ([39481d6](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/39481d61ab0f2dc1e4733b4d16d1125eb9527df2))


### Bug Fixes

* correction of fab button ([34980f0](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/34980f00a3b6e8f5ccd927d3af96d98896a841d9))
* feedback form working again ([081d209](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/081d209b61a9c7a6426f928e84cd677ac00943f1))
* resolve ng-deep by global ([6418dab](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/6418dab4885dbf71ca30fe37830ec3426dd2cb00))
* resolved deprecation warning ([1479049](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/14790492f74f2ee684ca69e75c9eb80bb019dcfe))

### [0.9.6](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/compare/v0.9.5...v0.9.6) (2022-05-27)


### Features

* [#38](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/issues/38) back to login button ([08b4151](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/08b4151cac73051fc42c5a3bb5b9f034de83f262))
* api_key from swagger for stats ([a305a6d](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/a305a6d9d07486f700427ef5c3fce9285f8ddfb6))
* featureflag service ([92eae68](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/92eae688e6609b628bd9525434e2b96fd4e3f028))
* messages & translations ([3982328](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/3982328ad0dae5589dda5dfd444638b61cccbdbb))
* reorder frontpage elements ([d54823b](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/d54823bd8cdb76485896b1cb0083adefd5dfc593))
* switched to eslint ([cf5fbc4](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/cf5fbc4f7fc8aa841a615b42a49acbfaff252ae1))
* update of tankerkoenig api to 4.0.6 ([52c83bd](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/52c83bdc1fb9b4020dbe8e8ee58b5cee6e447275))


### Bug Fixes

* corrected import ([f4e9781](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/f4e9781f83f1f34fd0e143ee2f4b008fc9ba9699))
* enabling chartJS for ng12 + cleanup ([27fa8e8](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/27fa8e8c25c14e2c97f03168f9eaaa3595e516b7))
* fixed missed values after translation ([17b0945](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/17b0945e1e9e59718c2e0da99edb59b8c7c79b49))
* stars getting displayed correctly ([05d5f97](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/05d5f970649502585b199e6efe3516866f46ecc9))
* stats reenabled ([b0c69a3](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/b0c69a391a251a776da17e81d8bef655621362b8))
* stats will add a zero to price if needed ([de8938b](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/de8938b594ac07168a71bd7f7e01229c7618666b))

### [0.9.5](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/compare/v0.9.4...v0.9.5) (2021-01-02)


### Features

* [WIP] Login Basis - Routehandling etc fehlt noch ([cc5e036](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/cc5e03662a266c28f390cb40474530b46fb5ff3b))
* Login + Register implementiert ([3229901](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/322990155f809d59ac169ef37679f3c14dbee41c))
* Login mit Facebook ermöglicht ([9fd73df](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/9fd73df024d2eb0d880395a3c5e8f7033613b9e4))
* Login über Email & Google integriert ([1a39008](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/1a39008151cd580bdbb4a2e3bb399c2401170c25))
* Passwort vergessen Funktion ([c1dbec9](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/c1dbec9b796a28ab6cd1d9bef17a82cd0a65425b))
* SafePipe für Sanitizing ([cb5255b](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/cb5255b70a82c2111c94c382cc6ee67ee540cf53))
* Translation der Login/RegisterViews ([90f6e77](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/90f6e77763d55a60ddde4607b6bb47e52b7a57e6))


### Bug Fixes

* Checks korrigiert ([bd6027f](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/bd6027f087e43aade10481151928cf2d9fc21cc9))
* cleanup of routes ([a4cc39a](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/a4cc39a0fe5c8cdf3efdf2caa19afcae93fe8494))
* no empty cards anymore [#35](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/issues/35) ([6bea14b](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/6bea14b98d6266a46b01a83c1afb9bee232d6db9))
* Routing direkt im UserModule ([32e63a8](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/32e63a8076ccdeba863a5fa9052ffa885560a279))
* translatebutton visible again ([d9b52df](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/d9b52dfe72879bead347589a91ed93e3a348c23e))

### [0.9.4](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/compare/v0.9.3...v0.9.4) (2020-02-01)


### Features

* sidenav öffnet nun von rechts ([c3dd01c](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/c3dd01c28a6219292bb25f4b4bae8b110603cf59))


### Bug Fixes

* Buildfix ([5df32f3](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/commit/5df32f35f1f27092e4f4e9dc244e26e839da4c58))
