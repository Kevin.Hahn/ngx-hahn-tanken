[![pipeline status](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/badges/develop/pipeline.svg)](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/-/commits/develop) [![coverage report](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/badges/develop/coverage.svg)](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/-/commits/develop)
[![Latest Release](https://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/-/badges/release.svg)](http://gitlab.com/Kevin.Hahn/ngx-hahn-tanken/-/releases)
[![Bugs](http://5.181.51.30:9001/api/project_badges/measure?project=ngx-hahn-tanken&metric=bugs&token=184337ed333887f3989fa30857bc91be57a7e82f)](http://5.181.51.30:9001/dashboard?id=ngx-hahn-tanken)
[![Vulnerabilities](http://5.181.51.30:9001/api/project_badges/measure?project=ngx-hahn-tanken&metric=vulnerabilities&token=184337ed333887f3989fa30857bc91be57a7e82f)](http://5.181.51.30:9001/dashboard?id=ngx-hahn-tanken)
[![Security Rating](http://5.181.51.30:9001/api/project_badges/measure?project=ngx-hahn-tanken&metric=security_rating&token=184337ed333887f3989fa30857bc91be57a7e82f)](http://5.181.51.30:9001/dashboard?id=ngx-hahn-tanken)

# Hahn-Tanken

A project build with Angular to handle gas station prices. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Deployment

Deployment is automated, see .gitlab-ci.yml


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.